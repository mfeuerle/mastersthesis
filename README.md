# Mastersthesis
This is the implementation of my masters thesis "Algebraic Multigrid Methods for Kronecker Product Structures (Including Sums and Blocks)", Moritz Feuerle, 2022. The documentation of the `kron` module can be found [here](https://mfeuerle.gitlab.io/mastersthesis/).

## Installing
To install this project:
```
python3 -m virtualenv -p python3.9 venv
source venv/bin/activate
pip install --upgrade pip
pip install numpy matplotlib scipy pyqt5
```
Additionally, for 'mastersthesis/examples/tamg/load_stokes.py' to work:
```
cd <path to matlab>/extern/engines/python   # e.g. /usr/local/MATLAB/R2021b/extern/engines/python
python setup.py install
```
## Building the Docs
```
pip install sphinx pydata-sphinx-theme
cd docs
make html
```
