﻿kron.kronblock_matrix
=====================

.. currentmodule:: kron

.. autoclass:: kronblock_matrix
   :no-members:
   :no-inherited-members:
   :no-special-members:

   
   
   .. rubric:: Attributes

   
   
   .. autoattribute:: T
   .. autoattribute:: ndim
   .. autoattribute:: kdim
   .. autoattribute:: bshape
   .. autoattribute:: sbshape
   .. autoattribute:: sbkshape
   .. autoattribute:: dtype
   .. autoattribute:: shape
   
   

  
   
   .. rubric:: Methods

   .. autosummary::
      :toctree: generated/
   
      ~kronblock_matrix.__add__
      ~kronblock_matrix.__matmul__
      ~kronblock_matrix.__mul__
      ~kronblock_matrix.__neg__
      ~kronblock_matrix.__radd__
      ~kronblock_matrix.__rmatmul__
      ~kronblock_matrix.__rmul__
      ~kronblock_matrix.__rsub__
      ~kronblock_matrix.__sub__
      ~kronblock_matrix.ascollection
      ~kronblock_matrix.asformat
      ~kronblock_matrix.assemble
      ~kronblock_matrix.cfsplit
      ~kronblock_matrix.copy
      ~kronblock_matrix.diagonal
      ~kronblock_matrix.dot
      ~kronblock_matrix.eliminate_zeros
      ~kronblock_matrix.getklayer
      ~kronblock_matrix.hascollections
      ~kronblock_matrix.iscollection
      ~kronblock_matrix.matmat
      ~kronblock_matrix.matvec
      ~kronblock_matrix.multiply
      ~kronblock_matrix.rdot
      ~kronblock_matrix.rmatmat
      ~kronblock_matrix.rmatvec
      ~kronblock_matrix.sum
      ~kronblock_matrix.toarray
      ~kronblock_matrix.tobsr
      ~kronblock_matrix.tocoo
      ~kronblock_matrix.tocsc
      ~kronblock_matrix.tocsr
      ~kronblock_matrix.todense
      ~kronblock_matrix.todia
      ~kronblock_matrix.todok
      ~kronblock_matrix.tolil
      ~kronblock_matrix.transpose
   
  