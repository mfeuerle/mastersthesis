﻿kron.block_matrix
=================

.. currentmodule:: kron

.. autoclass:: block_matrix
   :no-members:
   :no-inherited-members:
   :no-special-members:

   
   
   .. rubric:: Attributes

   
   
   .. autoattribute:: T
   .. autoattribute:: ndim
   .. autoattribute:: bshape
   .. autoattribute:: sbshape
   .. autoattribute:: dtype
   .. autoattribute:: shape
   .. autoattribute:: kdim
   
   

  
   
   .. rubric:: Methods

   .. autosummary::
      :toctree: generated/
   
      ~block_matrix.__add__
      ~block_matrix.__matmul__
      ~block_matrix.__mul__
      ~block_matrix.__neg__
      ~block_matrix.__radd__
      ~block_matrix.__rmatmul__
      ~block_matrix.__rmul__
      ~block_matrix.__rsub__
      ~block_matrix.__sub__
      ~block_matrix.ascollection
      ~block_matrix.asformat
      ~block_matrix.assemble
      ~block_matrix.cfsplit
      ~block_matrix.copy
      ~block_matrix.diagonal
      ~block_matrix.dot
      ~block_matrix.eliminate_zeros
      ~block_matrix.getklayer
      ~block_matrix.hascollections
      ~block_matrix.iscollection
      ~block_matrix.matmat
      ~block_matrix.matvec
      ~block_matrix.multiply
      ~block_matrix.rdot
      ~block_matrix.rmatmat
      ~block_matrix.rmatvec
      ~block_matrix.sum
      ~block_matrix.toarray
      ~block_matrix.tobsr
      ~block_matrix.tocoo
      ~block_matrix.tocsc
      ~block_matrix.tocsr
      ~block_matrix.todense
      ~block_matrix.todia
      ~block_matrix.todok
      ~block_matrix.tolil
      ~block_matrix.transpose
   
  