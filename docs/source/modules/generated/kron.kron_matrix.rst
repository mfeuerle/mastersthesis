﻿kron.kron_matrix
================

.. currentmodule:: kron

.. autoclass:: kron_matrix
   :no-members:
   :no-inherited-members:
   :no-special-members:

   
   
   .. rubric:: Attributes

   
   
   .. autoattribute:: T
   .. autoattribute:: ndim
   .. autoattribute:: kdim
   .. autoattribute:: kshape
   .. autoattribute:: dtype
   .. autoattribute:: shape
   
   

  
   
   .. rubric:: Methods

   .. autosummary::
      :toctree: generated/
   
      ~kron_matrix.__add__
      ~kron_matrix.__matmul__
      ~kron_matrix.__mul__
      ~kron_matrix.__neg__
      ~kron_matrix.__radd__
      ~kron_matrix.__rmatmul__
      ~kron_matrix.__rmul__
      ~kron_matrix.__rsub__
      ~kron_matrix.__sub__
      ~kron_matrix.asformat
      ~kron_matrix.assemble
      ~kron_matrix.cfsplit
      ~kron_matrix.copy
      ~kron_matrix.diagonal
      ~kron_matrix.dot
      ~kron_matrix.eliminate_zeros
      ~kron_matrix.getformat
      ~kron_matrix.getklayer
      ~kron_matrix.matmat
      ~kron_matrix.matvec
      ~kron_matrix.multiply
      ~kron_matrix.rdot
      ~kron_matrix.rmatmat
      ~kron_matrix.rmatvec
      ~kron_matrix.sum
      ~kron_matrix.toarray
      ~kron_matrix.tobsr
      ~kron_matrix.tocoo
      ~kron_matrix.tocsc
      ~kron_matrix.tocsr
      ~kron_matrix.todense
      ~kron_matrix.todia
      ~kron_matrix.todok
      ~kron_matrix.tolil
      ~kron_matrix.transpose
      ~kron_matrix.withformat
   
  