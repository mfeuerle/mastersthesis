﻿kron.kronsum_matrix
===================

.. currentmodule:: kron

.. autoclass:: kronsum_matrix
   :no-members:
   :no-inherited-members:
   :no-special-members:

   
   
   .. rubric:: Attributes

   
   
   .. autoattribute:: T
   .. autoattribute:: ndim
   .. autoattribute:: N
   .. autoattribute:: kdim
   .. autoattribute:: kshape
   .. autoattribute:: dtype
   .. autoattribute:: shape
   
   

  
   
   .. rubric:: Methods

   .. autosummary::
      :toctree: generated/
   
      ~kronsum_matrix.__add__
      ~kronsum_matrix.__matmul__
      ~kronsum_matrix.__mul__
      ~kronsum_matrix.__neg__
      ~kronsum_matrix.__radd__
      ~kronsum_matrix.__rmatmul__
      ~kronsum_matrix.__rmul__
      ~kronsum_matrix.__rsub__
      ~kronsum_matrix.__sub__
      ~kronsum_matrix.asformat
      ~kronsum_matrix.assemble
      ~kronsum_matrix.cfsplit
      ~kronsum_matrix.copy
      ~kronsum_matrix.diagonal
      ~kronsum_matrix.dot
      ~kronsum_matrix.eliminate_zeros
      ~kronsum_matrix.getklayer
      ~kronsum_matrix.matmat
      ~kronsum_matrix.matvec
      ~kronsum_matrix.multiply
      ~kronsum_matrix.rdot
      ~kronsum_matrix.rmatmat
      ~kronsum_matrix.rmatvec
      ~kronsum_matrix.sum
      ~kronsum_matrix.toarray
      ~kronsum_matrix.tobsr
      ~kronsum_matrix.tocoo
      ~kronsum_matrix.tocsc
      ~kronsum_matrix.tocsr
      ~kronsum_matrix.todense
      ~kronsum_matrix.todia
      ~kronsum_matrix.todok
      ~kronsum_matrix.tolil
      ~kronsum_matrix.transpose
   
  