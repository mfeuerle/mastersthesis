﻿kron.utils.MatrixCollection
===========================

.. currentmodule:: kron.utils

.. autoclass:: MatrixCollection
   :no-members:
   :no-inherited-members:
   :no-special-members:

   
   
   .. rubric:: Attributes

   
   
   .. autoattribute:: ndim
   .. autoattribute:: dtype
   .. autoattribute:: shape
   .. autoattribute:: data
   
   

  
   
   .. rubric:: Methods

   .. autosummary::
      :toctree: generated/
   
      ~MatrixCollection.__add__
      ~MatrixCollection.__matmul__
      ~MatrixCollection.__mul__
      ~MatrixCollection.__radd__
      ~MatrixCollection.__rmatmul__
      ~MatrixCollection.__rmul__
      ~MatrixCollection.append
   
  