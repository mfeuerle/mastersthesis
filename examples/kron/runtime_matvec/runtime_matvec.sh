#!/bin/bash
#
#SBATCH --output=runtime_matvec.out
#
#SBATCH --job-name=main_matvec
#SBATCH --time=72:00:00
#SBATCH --mem=100mb
#SBATCH --partition=single

set -e
# set -x

module load devel/python

folder=${1:-data}
max_jobs=${2:-100}

slurm_folder="${folder}/slurmlog/"

mkdir -p ${slurm_folder}
mkdir -p ${folder}


for n in {5000..1500..-500} {1000..600..-100} {500..150..-50} {100..60..-10} {50..15..-5} {14..2..-1} ; do
    if [ "$n" -lt 16 ] ; then
        Kmax=15
    elif [ "$n" -lt 1000 ] ; then
        Kmax=5
    else
        Kmax=3
    fi
    for K in $(seq 2 $Kmax) ; do
        for format in dense sparse kron-dense kron-sparse ; do
            job_name="${format}_${n}_${K}"
            slurm_file="${slurm_folder}${job_name}.dat"
            result_file="${folder}/${format}.dat"

            # inseart headline
            if [ ! -f "$result_file" ] ; then
                echo "# format,n,K,N,time" > $result_file
            fi

            # prevent calculation of the same setting multiple times
            if ! grep -q "${format},${n},${K}" "$result_file"; then

                until [ $(squeue -h| wc -l) != $max_jobs ]
                    do	
                    sleep 1s
                done

                output=$(sbatch --job-name=${job_name} --output=${slurm_file} runtime_matvec_job.sh ${format} ${n} ${K} ${result_file})
                printf 'format=%-6s n=%-2s K=%-6s:  %s\n' "${format}" "${n}" "${K}" "${output}"

            fi
        done
    done
done
