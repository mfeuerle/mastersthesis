import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).parent.resolve()) + '/../../../source/')

import kron
from kron._kron import tosparray
import numpy as np
from scipy import sparse
import time

if len(sys.argv) > 1:
    format = sys.argv[1]
    n  = int(sys.argv[2])
    K  = int(sys.argv[3])
else:
    format = 'kron-sparse'
    n = 5
    K = 2


sparse_density = 0.2
np.random.seed(n)
A = [tosparray(sparse.rand(n,n, density=sparse_density, format='csr')) for k in range(K)]
x = np.random.rand(n**K,1)
    
if 'dense' in format:
    A = [Ak.toarray() for Ak in A]
else:
    assert 'sparse' in format
    
A = kron.kron(A)
    
if format == 'sparse':
    A = A.tocsr()
elif format == 'dense':
    A = A.toarray()
else:
    assert 'kron' in format


start_time = time.time()
A @ x
print(f"{format},{n},{K},{n**K},{time.time() - start_time}")
    


    