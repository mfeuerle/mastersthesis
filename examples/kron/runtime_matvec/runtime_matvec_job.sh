#!/bin/bash
#
#SBATCH --time=4:00:00
#SBATCH --mem=175gb
#SBATCH --partition=single

if [ "$#" -lt 3 ]; then
    	echo "Illegal number of parameters"
else
    format=$1
    n=$2
    K=$3
    file=${4:-"tmp.dat"}

    touch $file
    
    python runtime_matvec.py $format $n $K >> $file
fi


