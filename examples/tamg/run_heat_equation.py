import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).parent.resolve()) + '/../../source/')

import numpy as np
from scipy.sparse.linalg import cg
from scipy._lib._threadsafety import ReentrancyError
from time import time
import cProfile
import matplotlib.pyplot as plt

from tamg import mg, smoother
from heat_equation import HeatEquation


def cg_wrapper(A, b, x0=None, *args, **kwargs):
    if b.ndim == 1 or b.shape[1] == 1:
        if x0 is None:
            x, flag = cg(A,b, *args, **kwargs)
        else:
            x, flag = cg(A,b,x0, *args, **kwargs)           
    else:
        x    = b.shape[1] * [None]
        flag = b.shape[1] * [0]
        next = lambda : kwargs['callback'].next() if 'callback' in kwargs else None
        if x0 is None:
            for i in range(b.shape[1]):
                next()
                x[i], flag[i] = cg(A,b[:,i], *args, **kwargs)
        else:
            for i in range(b.shape[1]):
                next()
                x[i], flag[i] = cg(A,b[:,i],x0[:,i], *args, **kwargs)    
        x = np.vstack(x).T
    return x, flag


class CallBack:
    def __init__(self, A, b, x_ref, name, one_line=False, to_file=False):
        self.iter = -1
        self.idx = None
        self.A = A
        self.b = b.reshape(-1)
        self.name = name
        self.norm_b = np.linalg.norm(b)
        self.end = '\r' if one_line else '\n'
        self.start_time = time()
        self.relres = []
        self.time = []
        self.x_ref = x_ref.reshape(-1)
        self.x_ref_norm = np.linalg.norm(x_ref)
        self.relerr = []
        self.to_file = to_file
        
        if to_file:
            import socket
            import os.path
            
            self.filename = __file__[:-3] + "_" + socket.gethostname() + "_" + self.name + ".dat"
            if os.path.isfile(self.filename):
                raise RuntimeError(f"data file '{self.filename}' already exists")
            else:
                with open(self.filename, 'w') as writer:
                    writer.write("iter,relerr,relres,time\n")

        
    def __call__(self, x):
        self.iter += 1
    
        relerr = np.linalg.norm(x - self.x_ref) / self.x_ref_norm
        relres = np.linalg.norm(self.b - self.A @ x) / self.norm_b
        runtime = time()-self.start_time
        
        if self.to_file:
            with open(self.filename, 'a') as writer:
                writer.write(f"{self.iter},{relerr},{relres},{runtime}\n")
                
        else:
            print(f"\t{self.name}: iteration {self.iter:5d}",end='')
            print(f": relerr {relerr : 5.4e}, relres {relres : 5.4e} [{runtime:7.2f}s]",end=self.end)
            self.relerr.append(relerr)
            self.relres.append(relres)
            self.time.append(runtime)
            
            
if __name__ == '__main__':            
    n = 30
    K = 30
    L = 4
    dim_rhs = 0

    one_line_output = True



    # matrix_type = ['kron', 'array', 'sparse']
    matrix_type         = 'kron'
    # dim_rhs = any integer > 0

    # cycle_type = ['F', 'V', 'W']
    cycle_type          = 'F'
    # init_cycle = ['N', 'F', 'V', 'W', '0']
    init_cycle          = 'V'
    # precon_init = [None, 'N', 'F', 'V', 'W', '0']
    precon_init         = None 
    # omega = any number > 0
    omega               = 1
    # pre_smoother = [smoother.SymmetricGaussSeidel(), smoother.ForwardGaussSeidel(), smoother.BackwardGaussSeidel(), lambda A,b,x0: x0, lambda A,b,x0: cg_wrapper(A,b,x0,maxiter=5)[0]]
    pre_smoother        = smoother.SymmetricGaussSeidel()
    # post_smoother = analog to pre_smoother or additionally None
    post_smoother       = 'pre'
    # coarsest_solver = ['direct', lambda A,b,x0: cg_wrapper(A,b,x0)[0], smoother.SymmetricGaussSeidel(10)]
    coarsest_solver     = 'direct'



    def right_hand(t,x):
        global dim_rhs
        if dim_rhs == 0:
            f = np.ones(shape=(len(t),len(x)))
        else:
            f = np.ones(shape=(len(t),len(x),dim_rhs))
        return f


    problem = HeatEquation(K,n, right_hand)


    A = problem.B
    b = problem.rhs
    x_ref = np.linalg.solve(A.toarray(), b)


    _A,_P,_R = problem.assemble_hirarchy(L)


    if matrix_type == 'kron':
        grid = mg.MultiGrid(_A,_P,_R)
    elif matrix_type == 'array':
        grid = mg.MultiGrid([A.toarray() for A in _A],[P.toarray() for P in _P],[R.toarray() for R in _R])
    elif  matrix_type == 'sparse':
        grid = mg.MultiGrid([A.assemble() for A in _A],[P.assemble() for P in _P],[R.assemble() for R in _R])


    A_mg = mg.MultiGridOperator(grid, pre_smoother, post_smoother, cycle_type, omega, coarsest_solver)


    if init_cycle == '0':
        x0 = init_cycle = np.zeros([A.shape[1]] + [i for i in b.shape[1:]])
    else:
        x0 = A_mg.get_initial_value(b, init_cycle)
        

    M = mg.MultiGridPreconditioner(A_mg, precon_init)
                
    
    results = []
                                
    print("\nVanilla CG:")
    print("----------------------------------------------------------")
    callback_cg = CallBack(A,b,"scipy cg",one_line_output)
    time_cg = time()
    x_cg, flag_cg = cg_wrapper(A, b, x0, callback=callback_cg)
    time_cg = time() - time_cg
    err_cg = np.linalg.norm(x_ref-x_cg)/np.linalg.norm(x_ref)
    results.append([time_cg, callback_cg, err_cg, flag_cg])
    print("\nResults:")
    print(f"\truntime    : {time_cg:5.2f} sec")
    print(f"\t#Iterations: {callback_cg.iter}")
    print(f"\trel. error : {err_cg:5.4e}")
    print(f"\texit flag  : {flag_cg}\n")

    # cProfile.run("cg_wrapper(A, b, x0, callback=callback_cg)",sort='tottime')



    print("\nFull Multigird:")
    print("----------------------------------------------------------")
    callback_mg = CallBack(A,b,"mgsolve",one_line_output)
    time_mg = time()
    x_mg, flag_mg = mg.mgsolve(A_mg, b, init_cycle, callback=callback_mg)
    time_mg = time() - time_mg
    err_mg = np.linalg.norm(x_ref-x_mg)/np.linalg.norm(x_ref)
    results.append([time_mg, callback_mg, err_mg, flag_mg])
    print("\nResults:")
    print(f"\truntime    : {time_mg:5.2f} sec")
    print(f"\t#Iterations: {callback_mg.iter}")
    print(f"\trel. error : {err_mg:5.4e}")
    print(f"\texit flag  : {flag_mg}\n")

    # cProfile.run("mg.mgsolve(A_mg, b, init_cycle, callback=callback_mg)",sort='tottime')



    print("\nPreconditioned CG (my implementation):")
    print("----------------------------------------------------------")
    callback_mgpcg = CallBack(A,b,"mgpcgsolve",one_line_output)
    time_mgpcg = time()
    x_mgpcg, flag_mgpcg = mg.mgpcgsolve(A_mg, b, init_cycle, M=M, callback=callback_mgpcg)
    time_mgpcg = time() - time_mgpcg
    err_mgpcg = np.linalg.norm(x_ref-x_mgpcg)/np.linalg.norm(x_ref)
    results.append([time_mgpcg, callback_mgpcg, err_mgpcg, flag_mgpcg])
    print("\nResults:")
    print(f"\truntime    : {time_mgpcg:5.2f} sec")
    print(f"\t#Iterations: {callback_mgpcg.iter}")
    print(f"\trel. error : {err_mgpcg:5.4e}")
    print(f"\texit flag  : {flag_mgpcg}\n")

    # cProfile.run("mg.mgsolve_pcg(A_mg, b, init_cycle, callback=callback_mgpcg, P_x0=precon_init)",sort='tottime')


    print("\nPreconditioned CG (scipy implementation):")
    print("----------------------------------------------------------")
    try:
        callback_pcg = CallBack(A,b,"scipy pcg",one_line_output)
        time_pcg = time()
        x_pcg, flag_pcg = cg_wrapper(A, b, x0, M=M, callback=callback_pcg)
        time_pcg = time() - time_pcg
        err_pcg = np.linalg.norm(x_ref-x_pcg)/np.linalg.norm(x_ref)
        results.append([time_pcg, callback_pcg, err_pcg, flag_pcg])
        print("\nResults:")
        print(f"\truntime    : {time_pcg:5.2f} sec")
        print(f"\t#Iterations: {callback_pcg.iter}")
        print(f"\trel. error : {err_pcg:5.4e}")
        print(f"\texit flag  : {flag_pcg}\n")
    except ReentrancyError:
        print("\tERROR: no nestet cg calls allowed!")

    # cProfile.run("cg_wrapper(A, b, x0, M=M, callback=callback_pcg)",sort='tottime')   
        
        
        
    if len(results) > 0:
        print("+--------------------------------------------------------+")
        print("|                        SUMMARY                         |")
        print("+-------------+---------+-------+------------+-----------+")
        print("| solver name | runtime | #iter | rel. error | exit flag |")
        print("+-------------+---------+-------+------------+-----------+")
        for res in results:
            print(f"| {res[1].name:11s} ",end='')
            print(f"| {res[0]:7.2f} ",end='')
            print(f"| {res[1].iter:5d} ",end='')
            print(f"| {res[2]:6.4e} ",end='')
            if isinstance(res[3],list):
                print("| ",end='')
                for i in res[3]: print(f"{i:1} ",end='')
                print("|")
            else:
                print(f"| {res[3]:9} |")
        print("+-------------+---------+-------+------------+-----------+")