#!/bin/bash

module load devel/python

# set -x
set -e
# sbatch -p single --time=72:00:00 --mem=1gb --output=runtime_stokes.out runtime_stokes.sh
# scancel -u $USER

#SBATCH --output=runtime_stokes.out

#SBATCH --job-name=main
#SBATCH --time=72:00:00
#SBATCH --mem=1gb
#SBATCH --partition=single

folder=$1
max_jobs=${2:-100}

mkdir ${folder}

for K in 020 030 040 050 075 100 125 150 175 200 ; do
	for nx in 05 08 12 18 22 ; do
		for format in dense sparse kron ; do
# for K in 100 150 200 250 300 350 ; do
# 	for nx in 25 30 35 40 45 50 ; do
# 		for format in sparse kron ; do
			for solver in direct cg pcg ; do
				if [ $format != "kron" ] || [ $solver != "direct" ]; then 
					file_name="${folder}/K${K}_nx${nx}/"
					mkdir -p ${file_name}
					file_name="${file_name}${format}_${solver}.dat"
					job_name="${solver},${format},${K},${nx}"

					until [ $(squeue -h| wc -l) != $max_jobs ]
				       	do	
						sleep 1s
					done
					
					output=$(sbatch --job-name=${job_name} --output=${file_name} runtime_stokes_job.sh ${K} ${nx} ${format} ${solver})
					printf 'K=%s nx=%s format=%-6s solver=%-6s:  %s\n' "${K}" "${nx}" "${format}" "${solver}" "${output}"
				fi
			done
		done
	done
done
