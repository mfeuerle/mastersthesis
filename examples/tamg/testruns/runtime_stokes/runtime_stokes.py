import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).parent.resolve()) + '/../../../../source/')
sys.path.append(str(pathlib.Path(__file__).parent.resolve()) + '/../../')

import numpy as np
import scipy.sparse
import scipy.sparse.linalg
from time import time
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')

import cProfile
import pstats
from pstats import SortKey

import kron
from kron.utils import tosparray

from tamg import mg
from tamg.amg.strength import *
import tamg.amg.interpol as interpol
from tamg.amg.construct import *

from load_stokes import load_stokes
from tamg.utils import CallBackLogging


if len(sys.argv) > 1:
    K  = int(sys.argv[1])
    nx = int(sys.argv[2])
    format = sys.argv[3]
    solver = sys.argv[4]
else:
    K = 50
    nx = 10
    format = 'kron'
    solver = 'pcg'
    
tol = 1e-5
    

        
#######################################################
# Stoks
#######################################################
I = [0,1]
ny = nx
m = 1
q = 1
B_stokes, space_data, time_data = load_stokes(I, K, nx, ny, m, q)
E_stokes,A_stokes,V_stokes = [tosparray(space_data.get(k)) for k in ['E','A','V']]
K_dt_stokes,L_dt_stokes,O_dt_stokes = [tosparray(time_data.get(k)) for k in ['K_dt','L_dt','O_dt']]
    
EE_stokes = E_stokes @ E_stokes.T
EA_stokes = E_stokes @ A_stokes.T
AA_stokes = A_stokes @ A_stokes.T
AAV_stokes = AA_stokes @ V_stokes
EAV_stokes = EA_stokes @ V_stokes
VAAV_stokes = V_stokes.T @ AAV_stokes

B11_stokes = kron.kron(K_dt_stokes[0:-1, 0:-1], EE_stokes) \
    + kron.kron(O_dt_stokes[0:-1, 0:-1], EA_stokes) \
    + kron.kron(O_dt_stokes[0:-1, 0:-1].T, EA_stokes.T) \
    + kron.kron(L_dt_stokes[0:-1, 0:-1], AA_stokes)        
B12_stokes = kron.kron(O_dt_stokes[0:-1, [-1]], EAV_stokes) \
    + kron.kron(L_dt_stokes[0:-1, [-1]], AAV_stokes)        
B21_stokes = kron.kron(O_dt_stokes[0:-1, [-1]].T, EAV_stokes.T) \
    + kron.kron(L_dt_stokes[[-1], 0:-1], AAV_stokes.T)    
B22_stokes = kron.kron(L_dt_stokes[-1,-1], VAAV_stokes)    
B_stokes = kron.block([[B11_stokes, B12_stokes], [B21_stokes, B22_stokes]])

    
##################################################
# Test problem
##################################################
A = B_stokes
stokes_st_block = kron.block([[kron.kron(K_dt_stokes[0:-1, 0:-1], AA_stokes), kron.kron(K_dt_stokes[0:-1, [-1]], AAV_stokes)], [kron.kron(K_dt_stokes[[-1], 0:-1], AAV_stokes.T), kron.kron(K_dt_stokes[-1,-1], VAAV_stokes) ]])


##################################################
# exact solution
##################################################
np.random.seed(A.shape[1])
# x_ref = np.random.rand(A.shape[1])
# b = A @ x_ref
b = np.random.rand(A.shape[1])


##################################################
# AMG settings
##################################################
factory_time = AMGFactory(Nmin=10, todense=(0,0.8), sbNmin=3)
factory_time.default_matrix(RugeStubenAbs(), StandardCoarsening(), StandardInterpolation())
factory_time.block_matrix(BlockMatrixStrength(separate=False), BlockMatrixCoarsening(), BlockMatrixInterpolation(interpol.OptimalInterpolation(),onlydiag=True))

factory_space = AMGFactory(Nmin=10, todense=(0,0.8), sbNmin=3)
factory_space.default_matrix(RugeStubenAbs(), StandardCoarsening(), StandardInterpolation())
factory_space.block_matrix(BlockMatrixStrength(separate=True), BlockMatrixCoarsening(), BlockMatrixInterpolation(interpol.OptimalInterpolation(),onlydiag=True))

factory_kron = AMGKronFactory([factory_time, factory_space], Nmin=50, todense=(0,0.8))


factor_sparse_dense = factory_time = AMGFactory(RugeStubenAbs(), StandardCoarsening(), StandardInterpolation(), Nmin=50, todense=(0,0.8))


##################################################
# logging
##################################################
print("#\n# stokes problem")
print(f"# K={K}")
print(f"# nx={nx}")
print(f"# ny={nx}")
print(f"# n={EE_stokes.shape[1]}")
print(f"# N={B_stokes.shape[1]}")
print(f"# format={format}")
print(f"# solver={solver}")
print(f"# tol={tol}\n#")


##################################################
# solving
##################################################

if format == 'dense':
    A = A.toarray()
elif format == 'sparse':
    A = A.tocsr()
elif format == 'kron':
    pass
else:
    raise ValueError(f"unknown format {format}")
    


time_start = time()

if solver == 'direct':
    
    x = scipy.sparse.linalg.spsolve(A,b) if issparse(A) else np.linalg.solve(A,b)
    
elif solver == 'cg':
    callback = CallBackLogging(A,b)
    x,flag = scipy.sparse.linalg.cg(A, b, tol=tol, callback=callback)
    
elif solver == 'pcg':
    
    if format == 'kron':
        A_mg = auto_assemble_kron(stokes_st_block, 30, factory_kron, kmode='sequentially', bmode=['all', 'all'])
        A_mg = mg.MultiGrid(A, P=A_mg.P)
    else:
        A_mg = auto_assemble(A, 30, factor_sparse_dense)
    MGO = mg.MultiGridOperator(A_mg, cycle_type='V')
    # x0 = MGO.get_initial_value(b,'W')
    M = mg.MultiGridPreconditioner(MGO)
    setup_amg = time() - time_start
    
    callback = CallBackLogging(A,b)
    x,flag = scipy.sparse.linalg.cg(A, b, M=M, tol=tol, callback=callback)
    
else:
    raise ValueError(f"unknown solver {solver}")

tot_time = time() - time_start


##################################################
# logging
##################################################
print("#")
if solver != 'direct':
    print(f"# flag={flag}")
    print(f"# iter={callback.iter}")
    print(f"# res={callback.res}")
    print(f"# relres={callback.relres}")
    if callback.x_ref is not None:
        print(f"# err={callback.err}")
        print(f"# relerr={callback.relerr}")
else:
    res = np.linalg.norm(A@x - b)
    relres = res / np.linalg.norm(b)
    print(f"# res={res}")
    print(f"# relres={relres}")
if solver == 'pcg':
    print(f"# amg_layers={len(A_mg)}")
    print(f"# time_setup_amg={setup_amg}")
    print(f"# time_solve={tot_time-setup_amg}")
print(f"# time_total={tot_time}")