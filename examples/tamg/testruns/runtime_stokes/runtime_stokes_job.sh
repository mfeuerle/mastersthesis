#!/bin/bash

#SBATCH --time=72:00:00
#SBATCH --mem=64gb
#SBATCH --partition=single

if [ "$#" -ne 4 ]; then
    	echo "Illegal number of parameters"
else
	python runtime_stokes.py $1 $2 $3 $4
fi

