import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).parent.resolve()) + '/../../source/')

import numpy as np
import scipy
import scipy.integrate
from scipy.sparse.linalg import cg
from scipy import sparse
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')

import kron

class HeatEquation:
    
    def __init__(self, K, n, f):
        x = np.linspace(0,1,n+1)
        t = np.linspace(0,1,K+1)
        
        h = 1/n
        delta_t = 1/K
        
        K_h = sparse.diags([-1, 2, -1], [-1, 0, 1], shape=(n+1,n+1), format='csr')
        K_h[0,0] = K_h[-1,-1] = 1
        K_h *= 1/h
        
        L_h = sparse.diags([1, 4, 1], [-1, 0, 1], shape=(n+1,n+1), format='csr')
        L_h[0,0] = L_h[-1,-1] = 2
        L_h *= h/6
        
        K_dt = sparse.diags([-1, 2, -1], [-1, 0, 1], shape=(K+1, K+1), format='csr')
        K_dt[0,0] = K_dt[-1,-1] = 1
        K_dt *= 1/delta_t
    
        L_dt = sparse.diags([1, 4, 1], [-1, 0, 1], shape=(K+1, K+1), format='csr')
        L_dt[0,0] = L_dt[-1,-1] = 2
        L_dt *= delta_t/6
    
        O_dt = sparse.diags([0.5, -0.5], [-1, 1], shape=(K+1, K+1), format='lil')
        O_dt[0,0] = -0.5
        O_dt[-1,-1] = 0.5
        O_dt = O_dt.tocsr()
        
        E =  L_h[1:-1,1:-1]
        A = -K_h[1:-1,1:-1]
        
        EE = E @ E.T
        EA = E @ A.T
        AA = A @ A.T
        
        B = kron.kron([K_dt[:-1, :-1], EE]) \
            + kron.kron([O_dt[:-1, :-1], EA]) \
            + kron.kron([O_dt[:-1, :-1].T, EA.T]) \
            + kron.kron([L_dt[:-1, :-1], AA])        

        F = f(t,x)
        rhs = kron.kron([L_dt[:-1, :], L_h[1:-1,:]]) @ F.reshape([-1] + [i for i in F.shape[2:]])
        
        self.x = x
        self.h = h
        self.t = t
        self.delta_t = delta_t
        
        self.K = K
        self.n = n
        
        self.f = f
        
        self.B = B
        self.rhs = rhs
        
        self.E = E
        self.A = A
        
        self.M_h = L_h[1:-1,1:-1]
        self.A_h = K_h[1:-1,1:-1]
        
        self.L_h = L_h
        
        
    def solve(self, reconstruct=False):
        u = sparse.linalg.cg(self.B, self.rhs)[0]
        # diff = u - np.linalg.solve(self.B.toarray(), self.rhs)
        # assert np.linalg.norm(diff) < 1e-5 * np.linalg.norm(self.B.toarray())
        
        if reconstruct:
            u = self.reconstruct_solution(u)
        
        return self.t, self.x, u
    
    def reconstruct_solution(self,u):
        # nicht wirklich richtig, sprungfunktionen nicht wirklich berücksichtigt
        u = u.reshape(self.K,self.n-1)
        U = np.zeros(shape=(self.K+1,self.n+1))
        for k in range(1,self.K-1):
            U[k,1:-1] = (self.E.T @ u[k-1]/self.delta_t - 2*self.A.T @ u[k] - self.E.T @ u[k+1]/self.delta_t)/2
        U[-2,1:-1] = (self.E.T @ u[-2]/self.delta_t - 2*self.A.T @ u[-1])/2
        U[-1,1:-1] = self.E.T @ u[-1]/self.delta_t
        U = U.T
        return U
    
    def solve_timestepping(self):
        MA = np.linalg.solve(self.M_h.toarray(),self.A_h.toarray())
        Mb = np.linalg.solve(self.M_h.toarray(),self.L_h[1:-1,:].toarray())
        fun = lambda t,u: Mb @ self.f([t],self.x).reshape(-1) - MA @ u
        u0 = np.zeros(shape=(self.n-1,))
        sol = scipy.integrate.solve_ivp(fun, [0,1], u0)
        
        u = np.zeros(shape=(self.n+1,sol['y'].shape[1]))
        u[1:-1,:] = sol['y']
        return sol['t'], self.x, u
    
    def plot_solution(self):
        fig = plt.figure()
        ax = plt.axes(projection='3d')

        t,x,U = self.solve(reconstruct=True)
        T, X = np.meshgrid(t, x)
        ax.plot_surface(T,X,U,  alpha=.5)

        t,x,U_ts = self.solve_timestepping()
        T, X = np.meshgrid(t, x)
        ax.plot_surface(T,X,U_ts,  alpha=.5)

        ax.set_xlabel('t')
        ax.set_ylabel('x')
        ax.set_zlabel('u(t,x)')
        plt.show()
        
        
    def assemble_hirarchy(self,L):
        B = [self.B]
        P = []
        R = []
        
        K,n = self.B.kshape[0]
        
        for l in range(L-1):
            P_l, R_l = self.get_projections(n,K)
            B_l = R_l @ B[-1] @ P_l
            K,n = B_l.kshape[0]
            B.append(B_l)
            P.append(P_l)
            R.append(R_l)
            
        return B, P, R
    
    
    def get_projections(self,n,K):
        P_dt = np.zeros((K, int(np.ceil((K-1)/2))))
        P_dt[0,0] = 0.5
        count = 0
        for i in range(1,K):
            if i % 2 == 0:
                P_dt[i,count] = 0.5
                if i < K-1: P_dt[i,count+1] = 0.5
                count += 1
            else:
                P_dt[i,count] = 1
        P_dt = sparse.csr_array(P_dt)
                
        P_h = np.zeros((n, int(np.ceil((n-1)/2))))
        P_h[0,0] = 0.5
        count = 0
        for i in range(1,n):
            if i % 2 == 0:
                P_h[i,count] = 0.5
                if i < n-1: P_h[i,count+1] = 0.5
                count += 1
            else:
                P_h[i,count] = 1
        P_h = sparse.csr_array(P_h)
        P = kron.kron([P_dt, P_h])
        return P, P.T