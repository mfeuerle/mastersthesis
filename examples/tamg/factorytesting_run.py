import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).parent.resolve()) + '/../../source/')

import numpy as np
import scipy.sparse
from scipy.sparse import issparse
import scipy.sparse.linalg
from time import time
import matplotlib.pyplot as plt

import cProfile
import pstats
from pstats import SortKey

import kron
from kron.utils import tosparray

from tamg.amg.strength import *
import tamg.amg.coarsen as coarsen
import tamg.amg.interpol as interpol
from tamg import mg, smoother
from tamg.amg.construct import *

from load_stokes import load_stokes
from tamg.utils import CallBack


np.set_printoptions(precision=5)


####################################################
# Heat equation
####################################################
K = 200

delta_t = 1/K
K_dt_stokes = tosparray(scipy.sparse.diags([-1, 2, -1], [-1, 0, 1], shape=(K, K), format='csr'))
K_dt_stokes[0,0] = 1
K_dt_stokes *= 1/delta_t

L_dt_stokes = tosparray(scipy.sparse.diags([1, 4, 1], [-1, 0, 1], shape=(K, K), format='csr'))
L_dt_stokes[0,0] = 2
L_dt_stokes *= delta_t/6

O_dt_stokes = tosparray(scipy.sparse.diags([0.5, -0.5], [-1, 1], shape=(K, K), format='lil'))
O_dt_stokes[0,0] = -0.5
O_dt_stokes = O_dt_stokes.tocsr()

E_heat =  L_dt_stokes[1:,1:]
A_heat = -K_dt_stokes[1:,1:]

EE_heat = E_heat @ E_heat.T
EA_heat = E_heat @ A_heat.T
AA_heat = A_heat @ A_heat.T

B_heat = kron.kron(K_dt_stokes, EE_heat) \
        + kron.kron(O_dt_stokes, EA_heat) \
        + kron.kron(O_dt_stokes.T, EA_heat.T) \
        + kron.kron(L_dt_stokes, AA_heat)
        
tim_stokes = [K_dt_stokes, O_dt_stokes, O_dt_stokes.T, L_dt_stokes]
Time_data_B = [M.toarray() for M in tim_stokes]

Space_data_A = [EE_heat, EA_heat, AA_heat]
Space_data_B = [M.toarray() for M in Space_data_A]        

        
#######################################################
# Stoks
#######################################################
I = [0,1]
K = 200
nx = 20
ny = 20
m = 1
q = 1

B_stokes, space_data, time_data = load_stokes(I, K, nx, ny, m, q)

E_stokes,A_stokes,V_stokes = [tosparray(space_data.get(k)) for k in ['E','A','V']]
K_dt_stokes,L_dt_stokes,O_dt_stokes = [tosparray(time_data.get(k)) for k in ['K_dt','L_dt','O_dt']]
    
EE_stokes = E_stokes @ E_stokes.T
EA_stokes = E_stokes @ A_stokes.T
AA_stokes = A_stokes @ A_stokes.T
AAV_stokes = AA_stokes @ V_stokes
EAV_stokes = EA_stokes @ V_stokes
VAAV_stokes = V_stokes.T @ AAV_stokes

B11_stokes = kron.kron(K_dt_stokes[0:-1, 0:-1], EE_stokes) \
    + kron.kron(O_dt_stokes[0:-1, 0:-1], EA_stokes) \
    + kron.kron(O_dt_stokes[0:-1, 0:-1].T, EA_stokes.T) \
    + kron.kron(L_dt_stokes[0:-1, 0:-1], AA_stokes)        
B12_stokes = kron.kron(O_dt_stokes[0:-1, [-1]], EAV_stokes) \
    + kron.kron(L_dt_stokes[0:-1, [-1]], AAV_stokes)        
B21_stokes = kron.kron(O_dt_stokes[0:-1, [-1]].T, EAV_stokes.T) \
    + kron.kron(L_dt_stokes[[-1], 0:-1], AAV_stokes.T)    
B22_stokes = kron.kron(L_dt_stokes[-1,-1], VAAV_stokes)    
B_stokes = kron.block([[B11_stokes, B12_stokes], [B21_stokes, B22_stokes]])


Z = kron.zeros(B22_stokes.kshape, format='csr')
B_stokes_collection = kron.block([[kron.kron(K_dt_stokes[0:-1, 0:-1], EE_stokes), None],[None, Z]]) \
                    + kron.block([[kron.kron(O_dt_stokes[0:-1, 0:-1], EA_stokes),  kron.kron(O_dt_stokes[0:-1, [-1]], EAV_stokes)],[None, Z]]) \
                    + kron.block([[kron.kron(O_dt_stokes[0:-1, 0:-1].T, EA_stokes.T), None],[kron.kron(O_dt_stokes[0:-1, [-1]].T, EAV_stokes.T), Z]]) \
                    + kron.block([[kron.kron(L_dt_stokes[0:-1, 0:-1], AA_stokes),  kron.kron(L_dt_stokes[0:-1, [-1]], AAV_stokes)],[kron.kron(L_dt_stokes[[-1], 0:-1], AAV_stokes.T), kron.kron(L_dt_stokes[-1,-1], VAAV_stokes)]])


    
##################################################
# Test problem
##################################################
A = B_stokes_collection

# A = kron.block([[kron.kron(L_dt_stokes[0:-1, 0:-1], AA_stokes), kron.kron(L_dt_stokes[0:-1, [-1]], AAV_stokes)], [kron.kron(L_dt_stokes[[-1], 0:-1], AAV_stokes.T), kron.kron(L_dt_stokes[-1,-1], VAAV_stokes) ]])
stokes_st_block = kron.block([[kron.kron(K_dt_stokes[0:-1, 0:-1], AA_stokes), kron.kron(K_dt_stokes[0:-1, [-1]], AAV_stokes)], [kron.kron(K_dt_stokes[[-1], 0:-1], AAV_stokes.T), kron.kron(K_dt_stokes[-1,-1], VAAV_stokes) ]])


##################################################
# AMG settings
##################################################
factory_time = AMGFactory(Nmin=10, todense=(0,0.8), sbNmin=3)
factory_time.default_matrix(RugeStubenAbs(), StandardCoarsening(), StandardInterpolation())
factory_time.block_matrix(BlockMatrixStrength(separate=False), BlockMatrixCoarsening(), BlockMatrixInterpolation(interpol.OptimalInterpolation(),onlydiag=True))
factory_time.blockcollection_matrix(BlockMatrixCollectionStrength( MatrixCollectionStrength(ncommon='any'), BlockMatrixStrength(separate=True) ), 
                                    BlockMatrixCollectionCoarsening( MatrixCollectionCoarsening(), BlockMatrixCoarsening() ), 
                                    BlockMatrixCollectionInterpolation( MatrixCollectionInterpolation(), BlockMatrixInterpolation(interpol.OptimalInterpolation(),onlydiag=True) ))
factory_time.collection_matrix(MatrixCollectionStrength(), MatrixCollectionCoarsening(), MatrixCollectionInterpolation())

factory_space = AMGFactory(Nmin=10, todense=(0,0.8), sbNmin=3)
factory_space.default_matrix(RugeStubenAbs(), StandardCoarsening(), StandardInterpolation())
factory_space.block_matrix(BlockMatrixStrength(separate=True), BlockMatrixCoarsening(), BlockMatrixInterpolation(interpol.OptimalInterpolation(),onlydiag=True))
factory_space.blockcollection_matrix(BlockMatrixCollectionStrength( MatrixCollectionStrength(ncommon='any'), BlockMatrixStrength(separate=True) ), 
                                    BlockMatrixCollectionCoarsening( MatrixCollectionCoarsening(), BlockMatrixCoarsening() ), 
                                    BlockMatrixCollectionInterpolation( MatrixCollectionInterpolation(), BlockMatrixInterpolation(interpol.OptimalInterpolation(),onlydiag=True) ))
factory_space.collection_matrix(MatrixCollectionStrength(), MatrixCollectionCoarsening(), MatrixCollectionInterpolation())

factory = AMGKronFactory([factory_time, factory_space], Nmin=50, todense=(0,0.8))

##################################################
# AMG construction
##################################################
# A_mg = auto_assemble_kron(kron.kron(K_dt_stokes, E_heat), Lmax=30)
# A_mg = mg.MultiGrid(A, P=A_mg.P)
# A_op = mg.MultiGridOperator(A_mg, cycle_type='V')
# M = mg.MultiGridPreconditioner(A_op)

A_mg = auto_assemble_kron(stokes_st_block, 30, factory, kmode='sequentially', bmode=['all', 'all'])
A_mg = mg.MultiGrid(A, P=A_mg.P)
A_op = mg.MultiGridOperator(A_mg, cycle_type='V')
M = mg.MultiGridPreconditioner(A_op)


##################################################


one_line_output = True    
b = np.ones((A.shape[1],))

np.random.seed(A.shape[1])
x_ref = np.random.rand(A.shape[1])
b = A @ x_ref


# print("\nPreconditioned CG (scipy):")
# callback_mgpcg = CallBack(A,b,"scipy pcg",one_line_output)
# time_mgpcg = time()
# with cProfile.Profile() as pr:
#     x_mgpcg, flag_mgpcg = scipy.sparse.linalg.cg(A, b, tol=1e-4, M=M, callback=callback_mgpcg, maxiter=10*A.shape[0])
# time_mgpcg = time() - time_mgpcg
# print("\n\tResults:")
# print(f"\t\truntime    : {time_mgpcg:5.2f} sec")
# print(f"\t\t#Iterations: {callback_mgpcg.iter}")
# print(f"\t\texit flag  : {flag_mgpcg}\n")
# plt.figure(1)
# plt.semilogy(callback_mgpcg.relres, label=callback_mgpcg.solver_name)
# plt.legend()
# plt.figure(2)
# plt.semilogy(np.array(callback_mgpcg.time)/1000,callback_mgpcg.relres, label=callback_mgpcg.solver_name)
# plt.legend()

# ps = pstats.Stats(pr).sort_stats('tottime')
# ps.print_stats()


print("\nVanilla CG:")
callback_cg = CallBack(A,b,"scipy cg",one_line_output)
time_cg = time()
x_cg, flag_cg = scipy.sparse.linalg.cg(A, b, tol=1e-4, callback=callback_cg, maxiter=10*A.shape[0])
time_cg = time() - time_cg
print("\n\tResults:")
print(f"\t\truntime    : {time_cg:5.2f} sec")
print(f"\t\t#Iterations: {callback_cg.iter}")
print(f"\t\texit flag  : {flag_cg}\n")
plt.figure(1)
plt.semilogy(callback_cg.relres, label=callback_cg.solver_name)
plt.legend()
plt.figure(2)
plt.semilogy(np.array(callback_cg.time)/1000,callback_cg.relres, label=callback_cg.solver_name)
plt.legend()

print()

# print("\nFull Multigird:")
# callback_mg = CallBack(A,b,"mgsolve",one_line_output)
# time_mg = time()
# x_mg, flag_mg = mg.mgsolve(A_op, b, callback=callback_mg)
# time_mg = time() - time_mg
# print("\n\tResults:")
# print(f"\t\truntime    : {time_mg:5.2f} sec")
# print(f"\t\t#Iterations: {callback_mg.iter}")
# print(f"\t\texit flag  : {flag_mg}")


plt.show()