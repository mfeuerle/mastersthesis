import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).parent.resolve()) + '/../../source/')

import numpy as np
import scipy.sparse
import scipy.sparse.linalg
from scipy.sparse import issparse
from time import time

import kron
from kron.utils import tosparray

from tamg.amg.strength import *
import tamg.amg.coarsen as coarsen
import tamg.amg.interpol as interpol
import tamg.amg.construct as construct
from tamg import mg, smoother

from load_stokes import load_stokes
from tamg.utils import CallBack


np.set_printoptions(precision=5)

####################################################
# Heat equation
####################################################
K = 10

delta_t = 1/K
K_dt = tosparray(scipy.sparse.diags([-1, 2, -1], [-1, 0, 1], shape=(K, K), format='csr'))
K_dt[0,0] = 1
K_dt *= 1/delta_t

L_dt = tosparray(scipy.sparse.diags([1, 4, 1], [-1, 0, 1], shape=(K, K), format='csr'))
L_dt[0,0] = 2
L_dt *= delta_t/6

O_dt = tosparray(scipy.sparse.diags([0.5, -0.5], [-1, 1], shape=(K, K), format='lil'))
O_dt[0,0] = -0.5
O_dt = O_dt.tocsr()

E_heat =  L_dt[1:,1:]
A_heat = -K_dt[1:,1:]

EE_heat = E_heat @ E_heat.T
EA_heat = E_heat @ A_heat.T
AA_heat = A_heat @ A_heat.T

B_heat = scipy.sparse.kron(K_dt, EE_heat) \
        + scipy.sparse.kron(O_dt, EA_heat) \
        + scipy.sparse.kron(O_dt.T, EA_heat.T) \
        + scipy.sparse.kron(L_dt, AA_heat)
        
#######################################################
# Stoks
#######################################################
# I = [0,1]
# K = 20
# nx = 15
# ny = 15
# m = 1
# q = 1

# B, space_data, time_data = load_stokes(I, K, nx, ny, m, q)

# E,A,V = [space_data.get(k) for k in ['E','A','V']]
# K_dt,L_dt,O_dt = [time_data.get(k) for k in ['K_dt','L_dt','O_dt']]
    
# EE = E @ E.T
# EA = E @ A.T
# AA = A @ A.T
# AAV = AA @ V
# EAV = EA @ V
# VAAV = V.T @ AAV

# B11 = scipy.sparse.kron(K_dt[0:-2, 0:-2], EE) \
#     + scipy.sparse.kron(O_dt[0:-2, 0:-2], EA) \
#     + scipy.sparse.kron(O_dt[0:-2, 0:-2].T, EA.T) \
#     + scipy.sparse.kron(L_dt[0:-2, 0:-2], AA)        
# B12 = scipy.sparse.kron(O_dt[0:-2, -1], EAV) \
#     + scipy.sparse.kron(L_dt[0:-2, -1], AAV)        
# B21 = scipy.sparse.kron(O_dt[0:-2, -1].T, EAV.T) \
#     + scipy.sparse.kron(L_dt[-1, 0:-2], AAV.T)    
# B22 = scipy.sparse.kron(L_dt[-1, -1], VAAV)    
# B = scipy.sparse.bmat([[B11, B12], [B21, B22]])


####################################################
# Random problems
####################################################

# N = 10000

# A_rnd = tosparray(scipy.sparse.rand(N,N, density=0.01))

A_rnd = np.random.random((5,5)) - 0.5
A_rnd[:,2] = 0


A = L_dt# + 0.001 * np.ones(K_dt.shape)



if issparse(A):
    A = tosparray(A).tocsr()
    B = A.toarray()
else:
    B = np.array(A)
    A = scipy.sparse.csr_array(B)
    
    
one_line_output = True    
b = np.ones((A.shape[1],))

time_ref = time()
x_ref = np.linalg.solve(A.toarray(), b)
time_ref = time() - time_ref
print(f"\nRefernce Solution: : {time_ref:5.2f} sec")

print("\nVanilla CG:")
callback_cg = CallBack(A,b,x_ref,"scipy cg",one_line_output)
time_cg = time()
x_cg, flag_cg = scipy.sparse.linalg.cg(A, b, callback=callback_cg, maxiter=10*A.shape[0])
time_cg = time() - time_cg
err_cg = np.linalg.norm(x_ref-x_cg)/np.linalg.norm(x_ref)
print("\n\tResults:")
print(f"\t\truntime    : {time_cg:5.2f} sec")
print(f"\t\t#Iterations: {callback_cg.iter}")
print(f"\t\trel. error : {err_cg:5.4e}")
print(f"\t\texit flag  : {flag_cg}\n")
    

coarse = coarsen.StandardCoarsening()

for strength in [RugeStubenAbs()]:
# for strength in [RugeStubenAbs(), RugeStubenAbsSymmetric(), CauchySchwarz1()]:
    print("\n============================================================\n")
    
    print(f"\nStrongness: {strength.__class__}, {strength.__dict__}")
    t=time(); SA = strength(A); print(f"\tSparse strengt: {time()-t: 6.3f}s")
    t=time(); SB = strength(B); print(f"\tDense  strengt: {time()-t: 6.3f}s")
    print(SA.toarray().astype(int))
    print(f"\tDifference: {np.linalg.norm(SA.toarray().astype(int)-SB.astype(int))}")
    
    print(f"\nCoarsening: {coarse.__class__}, {coarse.__dict__}")
    t=time(); cfsplitA = coarse(SA); print(f"\tSparse coarsening: {time()-t: 6.3f}s")
    t=time(); cfsplitB = coarse(SB); print(f"\tDense  coarsening: {time()-t: 6.3f}s")
    print(cfsplitA)
    print(f"\tDifference: {sum(cfsplitA != cfsplitB)}")

    for interpol in [interpol.OptimalInterpolation(), interpol.DirectInterpolation(scaling='universal',sparse=True), interpol.DirectInterpolation(scaling='diag',sparse=True), interpol.DirectInterpolation(scaling='rowsum',sparse=True), interpol.StandardInterpolation(scaling='rowsum',sparse=True)]:
    #for interpol in [interpol.StandardInterpolation(scaling='universal',sparse=True, eps_truncate=0), interpol.StandardInterpolation(scaling='rowsum',sparse=True, eps_truncate=0.2)]:
    # for interpol in [interpolation.UniversalInterpolation(order=i) for i in range(5)]:  
        print("\n------------------------------------------------------------\n")

        print(f"Interpolation: {interpol.__class__}, {interpol.__dict__}")
        t=time(); PA = interpol(A,SA,cfsplitA); print(f"\tSparse interpolation: {time()-t: 6.3f}s")
        t=time(); PB = interpol(B,SB,cfsplitB); print(f"\tDense  interpolation: {time()-t: 6.3f}s")
        print(PA.toarray())
        print(f"\tDifference: {np.linalg.norm(PA.toarray() - PB)}")
    
        # grid = mg.MultiGrid(A, [PA])
        # A_mg = mg.MultiGridOperator(grid, smoother.ChebyshevSmoother())
        # M = mg.MultiGridPreconditioner(A_mg)
        
        
        # print("\nFull Multigird:")
        # callback_mg = CallBack(A,b,x_ref,"mgsolve",one_line_output)
        # time_mg = time()
        # x_mg, flag_mg = mg.mgsolve(A_mg, b, callback=callback_mg)
        # time_mg = time() - time_mg
        # err_mg = np.linalg.norm(x_ref-x_mg)/np.linalg.norm(x_ref)
        # print("\n\tResults:")
        # print(f"\t\truntime    : {time_mg:5.2f} sec")
        # print(f"\t\t#Iterations: {callback_mg.iter}")
        # print(f"\t\trel. error : {err_mg:5.4e}")
        # print(f"\t\texit flag  : {flag_mg}")
        
        
        # print("\nPreconditioned CG (my implementation):")
        # callback_mgpcg = CallBack(A,b,x_ref,"mgpcgsolve",one_line_output)
        # time_mgpcg = time()
        # x_mgpcg, flag_mgpcg = mg.mgpcgsolve(A_mg, b, M=M, callback=callback_mgpcg)
        # time_mgpcg = time() - time_mgpcg
        # err_mgpcg = np.linalg.norm(x_ref-x_mgpcg)/np.linalg.norm(x_ref)
        # print("\n\tResults:")
        # print(f"\t\truntime    : {time_mgpcg:5.2f} sec")
        # print(f"\t\t#Iterations: {callback_mgpcg.iter}")
        # print(f"\t\trel. error : {err_mgpcg:5.4e}")
        # print(f"\t\texit flag  : {flag_mgpcg}\n")



