import glob
import os
import platform
import time
import warnings

import numpy as np
from scipy import sparse
from scipy.io.matlab import loadmat


def load_stokes(I, K, nx, ny, m, q):
    print('# Assamble time dependen stokes...')
    space_data = _load_space2(nx, ny, m, q)
    time_data = _load_time(I, K)
    B = _assemble_problem(space_data, time_data)
    
    print(f'#\tspace dofs: {space_data["E"].shape[1]}')
    print(f'#\ttotal dofs: {B.shape[1]}')
    
    return B, space_data, time_data


def _load_space2(nx, ny, m, q):
    # assamble file name
    dir_path = os.path.dirname(os.path.realpath(__file__))
    delim = '\\' if platform.system() == 'Windows' else '/'
    cache_folder = dir_path + delim + '.space_discretisations' 
    stokes_data_file = cache_folder + delim + 'stokes_data' + \
        '_nx'+str(nx)+'_ny'+str(ny)+'_m'+str(m)+'_q'+str(q) + \
        '.mat'
        
    os.makedirs(cache_folder, exist_ok=True)
        
    # Helper function: Calc discretisation via MATLAB and save as a .mat file
    def create_data_file():
        import matlab.engine
        print("#\tCalculate new spatial discretisations... ", end='', flush=True)
        start_time = time.time()
        mtlb = matlab.engine.start_matlab()
        mtlb.cd(dir_path, nargout=0)
        mtlb.workspace['stokes_data_file'] = stokes_data_file
        mtlb.workspace['nx'] = float(nx)
        mtlb.workspace['ny'] = float(ny)
        mtlb.workspace['m'] = float(m)
        mtlb.workspace['q'] = float(q)
        mtlb.evalc("global mu_choice; mu_choice = 3; global pmax; pmax = ceil(m/2);")
        mtlb.evalc("[E, A, B, C, v1xx, v2xx, pxx] = stokes_ind2(m, q, nx, ny);")
        mtlb.evalc("V = sparse(null(E')); d = size(V,2);")
        mtlb.evalc("clear mu_choice pmax")
        mtlb.evalc("save(stokes_data_file, '-regexp', ['^(?!', stokes_data_file,'$).'])")
        print(f'done. [{time.time() - start_time:.3f}s]')
        
    print("#\tSearching for cached spatial discretisation... ", end='', flush=True)
    try: 
        data = loadmat(stokes_data_file)
        print("Sucessfull!.")
    except FileNotFoundError:
        print("Unsucessfull!.")
        create_data_file()
        data = loadmat(stokes_data_file)
    
    if data['nx'] != nx or data['ny'] != ny or data['m'] != m or data['q'] != q:
        print("#\tError: Cached discretisation not matching.")
        os.remove(stokes_data_file)
        create_data_file()
        data = loadmat(stokes_data_file)
        
    return data



def _load_time(I, K):
    delta_t = (I[1]-I[0])/K
    t = np.linspace(I[0], I[1], K+1)

    K_dt = sparse.diags(1/delta_t*np.array([-1, 2, -1]), [-1, 0, 1], shape=(K+1, K+1), format='csc')
    K_dt[[0,-1],[0,-1]] = 1/delta_t
    
    L_dt = sparse.diags(delta_t/6*np.array([1, 4, 1]), [-1, 0, 1], shape=(K+1, K+1), format='csc')
    L_dt[[0,-1],[0,-1]] = delta_t/3
    
    O_dt = sparse.diags(np.array([0.5, -0.5]), [-1, 1], shape=(K+1, K+1), format='csc')
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        O_dt[[0,-1],[0,-1]] = [-0.5, 0.5]
    
    data = {"K_dt": K_dt,
            "L_dt": L_dt,
            "O_dt": O_dt,
            "I": I,
            "K": K,
            "t": t,
            "delta_t": delta_t
            }
    return data

def _assemble_problem(space_data, time_data):
    E,A,V = [space_data.get(k) for k in ['E','A','V']]
    K_dt,L_dt,O_dt = [time_data.get(k) for k in ['K_dt','L_dt','O_dt']]
    
    EE = E @ E.T
    EA = E @ A.T
    AA = A @ A.T
    AAV = AA @ V
    EAV = EA @ V
    VAAV = V.T @ AAV
    
    B11 = sparse.kron(K_dt[0:-1, 0:-1], EE) \
        + sparse.kron(O_dt[0:-1, 0:-1], EA) \
        + sparse.kron(O_dt[0:-1, 0:-1].T, EA.T) \
        + sparse.kron(L_dt[0:-1, 0:-1], AA)        
    B12 = sparse.kron(O_dt[0:-1, -1], EAV) \
        + sparse.kron(L_dt[0:-1, -1], AAV)        
    B21 = sparse.kron(O_dt[0:-1, -1].T, EAV.T) \
        + sparse.kron(L_dt[-1, 0:-1], AAV.T)    
    B22 = sparse.kron(L_dt[-1, -1], VAAV)    
    B = sparse.bmat([[B11, B12], [B21, B22]])
    
    # np.set_printoptions(precision=3)
    
    # print(np.linalg.norm(EE.toarray()))
    # print(np.linalg.norm(EA.toarray()))
    # print(np.linalg.norm(EA.T.toarray()))
    # print(np.linalg.norm(AA.toarray()))
    
    # print(np.linalg.norm((EE + EA + EA.T + AA).toarray()))
    
    # print(EE.toarray())
    # print(EA.toarray())
    # print(EA.T.toarray())
    # print(AA.toarray())
    
    # print((EE + EA + EA.T + AA).toarray())
    
    
    
    return B.tocsc()