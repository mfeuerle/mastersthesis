import numpy as np
import scipy.sparse
import scipy.linalg
from scipy.sparse import issparse
import scipy.sparse.linalg
from typing import overload

import kron
from kron import iskronrelated



class SmootherInterface:
    
    def __init__(self, iter, save):
        self.iter = iter
        self.save = save
        self._issetup = False
        
    def copy(self):
        dict_ = {k:v.copy() if hasattr(v,'copy') else v for k,v in self.__dict__.items() if not k.startswith('_')}
        return self.__class__(**dict_)
    
    
    def setup(self, A, *args, **kwargs):
        if self._issetup and id(self._A) == id(A): 
            return
        self._issetup = True
        self._A = A
        self._args = args
        self._kwargs = kwargs
        if self.save:
            self._setupdata = self._setup(A,*args,**kwargs)
        
    def _setup(self, A, *args, **kwargs):
        pass
        
        
    def apply(self, b, x0):
        if not self._issetup:
            raise RuntimeError("smoother used bevor setup was called")
        if self.save:
            return self._apply(b, x0, self._setupdata)
        else:
            return self._apply(b, x0, self._setup(self._A,*self._args,*self._kwargs))
        
    def _apply(self, b, x0, setupdata):
        raise NotImplementedError("This is an interface, please implement this method")
    
    
    @overload
    def __call__(self, b, x0):
        ...
    
    @overload
    def __call__(self, A, b, x0):
        ...
        
    def __call__(self, *args):
        if len(args) == 2:
            return self.apply(*args)
        elif len(args) == 3:
            self.setup(args[0])
            return self.apply(*args[1:])
        else:
            raise ValueError("call this eiter with (A,b,x0) or (b,x0)")
    
    

class Jacobi(SmootherInterface):
    
    def __init__(self, iter=2, save=False):
        super().__init__(iter, save)
    
    # def _setup(self, A, *args,**kwargs):
    #     d = A.diagonal()
    #     if iskronrelated(A):
    #         G = A - kron.diag(d)
    #         d = d.toarray()
    #     elif issparse(A):
    #         G = A - scipy.sparse.diags(d)
    #     else:
    #         G = A - np.diag(d)
    #     return G, d
        
    # def _apply(self, b, x0, setupdata):
    #     G,d = setupdata
    #     for i in range(self.iter):
    #         x0 = (b - G @ x0) / d
    #     return x0
    
    def _setup(self, A, *args,**kwargs):
        if iskronrelated(A):
            A = A.assemble()
        d = A.diagonal()
        if iskronrelated(d):
            d = d.toarray()
        d = d.reshape(-1)
        D = scipy.sparse.diags(d)
        return A, D, d
        
    def _apply(self, b, x0, setupdata):
        A,D,d = setupdata
        for i in range(self.iter):
            x0 = (b - (A@x0 - D@x0)) / d
        return x0
    
    # def get_spectral_radius(self, A):
    #     A = A.assemble()
    #     CJ = kron.eye(A.shape[0],A.shape[1]) - A / A.diagonal()
    #     # CJ = kron.eye(A.kshape[0], A.kshape[1]) - A / A.diagonal()
    #     # return scipy.linalg.eigvalsh(CJ,subset_by_index=[A.shape[0]-1,A.shape[0]-1])
    #     # return np.linalg.eigvals(CJ)
    #     # np.linalg.cholesky(A.toarray())
    #     return np.max(np.abs([scipy.linalg.eigvalsh(CJ, subset_by_index=[0,0]),scipy.linalg.eigvalsh(CJ, subset_by_index=[CJ.shape[0]-1,CJ.shape[0]-1])]))
        

class ForwardGaussSeidel(SmootherInterface):
    
    def __init__(self, iter=2, save=False):
        super().__init__(iter, save)
    
    def _setup(self, A, *args,**kwargs):
        if iskronrelated(A):
            A = A.assemble()
            LD = scipy.sparse.tril(A,format='csr')
            R  = scipy.sparse.triu(A,k=1)        
        elif issparse(A):
            LD = scipy.sparse.tril(A,format='csr')
            R  = scipy.sparse.triu(A,k=1)     
        else:
            LD = np.tril(A)
            R  = np.triu(A,k=1)
        return LD,R
    
    def _apply(self, b, x0, setupdata):
        LD,R = setupdata
        solve_triangular = scipy.sparse.linalg.spsolve_triangular if issparse(LD) else scipy.linalg.solve_triangular
        for i in range(self.iter):
            x0 = solve_triangular(LD, b - R@x0, lower=True)
        return x0
        
    # def get_spectral_radius(self, A):
    #     A = A.assemble()
    #     LD = scipy.sparse.tril(A,format='csr')
    #     R = scipy.sparse.triu(A,k=1)
    #     CGS = - spsolve_triangular(LD, R.toarray(), lower=True)
    #     return np.max(np.abs([scipy.linalg.eigvalsh(CGS, subset_by_index=[0,0]),scipy.linalg.eigvalsh(CGS, subset_by_index=[CGS.shape[0]-1,CGS.shape[0]-1])]))
    

class BackwardGaussSeidel(SmootherInterface):
    
    def __init__(self, iter=2, save=False):
        super().__init__(iter, save)
    
    def _setup(self, A, *args,**kwargs):
        if iskronrelated(A):
            A = A.assemble()
            RD = scipy.sparse.triu(A,format='csr')
            L  = scipy.sparse.tril(A,k=-1)                
        elif issparse(A):
            RD = scipy.sparse.triu(A,format='csr')
            L  = scipy.sparse.tril(A,k=-1)                
        else:
            RD = np.triu(A)
            L  = np.tril(A,k=-1)
        return RD,L
    
    def _apply(self, b, x0, setupdata):
        RD,L = setupdata
        solve_triangular = scipy.sparse.linalg.spsolve_triangular if issparse(RD) else scipy.linalg.solve_triangular
        for i in range(self.iter):
            x0 = solve_triangular(RD, b - L@x0, lower=False)
        return x0
        
    # def get_spectral_radius(self, A):
    #     A = A.assemble()
    #     RD = scipy.sparse.triu(A,format='csr')
    #     L = scipy.sparse.tril(A,k=-1)
    #     CGS = - spsolve_triangular(RD, L.toarray(), lower=False)
    #     return np.max(np.abs([scipy.linalg.eigvalsh(CGS, subset_by_index=[0,0]),scipy.linalg.eigvalsh(CGS, subset_by_index=[CGS.shape[0]-1,CGS.shape[0]-1])]))
    
    
class SymmetricGaussSeidel(SmootherInterface):
    
    def __init__(self, iter=2, save=False):
        super().__init__(iter, save)
    
    def _setup(self, A, *args,**kwargs):
        if iskronrelated(A):
            A = A.assemble()
            LD = scipy.sparse.tril(A,format='csr')
            RD = scipy.sparse.triu(A,format='csr')
            R  = scipy.sparse.triu(A,k=1)
            L  = scipy.sparse.tril(A,k=-1)                
        elif issparse(A):
            LD = scipy.sparse.tril(A,format='csr')
            RD = scipy.sparse.triu(A,format='csr')
            R  = scipy.sparse.triu(A,k=1)
            L  = scipy.sparse.tril(A,k=-1)                
        else:
            LD = np.tril(A)
            RD = np.triu(A)
            R  = np.triu(A,k=1)
            L  = np.tril(A,k=-1)
        return LD,RD,R,L
    
    def _apply(self, b, x0, setupdata):
        LD,RD,R,L = setupdata
        solve_triangularLD = scipy.sparse.linalg.spsolve_triangular if issparse(LD) else scipy.linalg.solve_triangular
        solve_triangularRD = scipy.sparse.linalg.spsolve_triangular if issparse(RD) else scipy.linalg.solve_triangular
        for i in range(self.iter):
            x0 = solve_triangularLD(LD, b - R@x0, lower=True)
            x0 = solve_triangularRD(RD, b - L@x0, lower=False)
        return x0        
        
    # def get_spectral_radius(self, A):
    #     A = A.assemble()
    #     LD = scipy.sparse.tril(A,format='csr')
    #     L = scipy.sparse.tril(A,k=-1)
    #     D = scipy.sparse.diags(A.diagonal())
    #     RD = scipy.sparse.triu(A,format='csr')
    #     R  = scipy.sparse.triu(A,k=1)
        
    #     # # this value has to be below 1
    #     CSGS = spsolve_triangular(RD,L @ spsolve_triangular(LD, R.toarray(), lower=True), lower=False)
    #     rho1 = np.max(np.abs([scipy.linalg.eigvalsh(CSGS, subset_by_index=[0,0]),scipy.linalg.eigvalsh(CSGS, subset_by_index=[CSGS.shape[0]-1,CSGS.shape[0]-1])]))
    
    #     # this value has to be greater then 0, see Theorem 3.6. https://www.math.uci.edu/~chenlong/226/Ch6IterativeMethod.pdf
    #     BA = spsolve_triangular(RD, D @ spsolve_triangular(LD, A.toarray(), lower=True), lower=False)
    #     rho2 = scipy.linalg.eigvalsh(BA,subset_by_index=[0,0])
    #     return [rho1, rho2[0]]
           
    
class ChebyshevSmoother(SmootherInterface):
    # only for syymetric matricies, otherwise a ellipsoid containing the eigenvalues is needed
    # Manteuffel T. The Tchebychev iteration for nonsymmetric linear systems. Numerische Mathematik 1977 section 2.5
    
    save = True
    
    def __init__(self, iter=2, power_iter=10, power_rtol=1e-8, estimate_factors=(1.1,0.03), max_eig_estimate=None, min_eig_estimate=None, save=True):
        assert save == self.save
        super().__init__(iter, self.save)
        self.power_iter = power_iter
        self.power_rtol = power_rtol
        self.estimate_factors = estimate_factors
        self.max_eig_estimate = max_eig_estimate
        self.min_eig_estimate = min_eig_estimate
        
    
    def _setup(self, A, *args,**kwargs):  
        if self.max_eig_estimate is None:
            max_eig_estimate = self.estimate_factors[0] * self._power_iteration(A)
        else:
            max_eig_estimate = self.max_eig_estimate
        if self.min_eig_estimate is None:
            min_eig_estimate = self.estimate_factors[1] * max_eig_estimate
        else:
            min_eig_estimate = self.min_eig_estimate
        d = 0.5 * (max_eig_estimate + min_eig_estimate)
        c = 0.5 * (max_eig_estimate - min_eig_estimate)
        return A,d,c
    
    def _apply(self, b, x0, setupdata):
        A,d,c = setupdata
        r = b - A @ x0
        delta = r/d
        x0 = x0 + delta
        alpha = 2*d/(2*d**2-c**2)
        beta = d*alpha - 1
        for i in range(self.iter):
            r = b - A @ x0
            delta = alpha*r + beta*delta
            alpha = 1/(d - alpha/4*c**2)
            beta = d*alpha - 1
        return x0
    
    
    def _power_iteration(self, A):
        x = np.random.rand(A.shape[1])
        x = x/np.linalg.norm(x)
        lmbd_old = 1
        
        for i in range(self.power_iter):
            y = A @ x
            lmbd = y.T @ x
            if abs(lmbd_old-lmbd)/abs(lmbd_old) < self.power_rtol:
                break
            x = y / np.linalg.norm(y)
            lmbd_old = lmbd
        return lmbd

class DirectSolver(SmootherInterface):
    
    def __init__(self, dense=None, save=True, iter=None):
        assert iter is None
        super().__init__(None, save)
        self.dense=dense
        
    def _setup(self, A, *args,**kwargs):
        if iskronrelated(A):
            if self.dense is None:
                A = A.tocsr()
            elif self.dense:
                A = A.toarray()
            else:
                A = A.tocsr()
        elif issparse(A):
            if self.dense is None:
                A = A if A.format=='csr' or A.format=='csc' else A.tocsr()
            elif self.dense:
                A = A.toarray()
            else:
                A = A if A.format=='csr' or A.format=='csc' else A.tocsr()
        else:
            if self.dense == False:
                A = scipy.sparse.csr_array(A)
        return A
    
    def _apply(self, b, x0, A):
        if issparse(A):
            return scipy.sparse.linalg.spsolve(A,b)
        else:
            return np.linalg.solve(A,b)
    
    
class ApproxOptimalSmoothing(SmootherInterface):
    
    def __init__(self, smoother=ChebyshevSmoother(), save=True, iter=None):
        assert iter is None
        super().__init__(None, save)
        self.smoother = smoother
        
    def _setup(self, A, C, *args, **kwargs):
        
        if kron.iskronrelated(A):
            AFF = A.cfsplit('F','F', C)
            AFC = A.cfsplit('F','C', C)
            C = C.toarray().reshape(-1)
            F = np.bitwise_not(C)
        else:
            C = C.reshape(-1)
            F = np.bitwise_not(C)
            AFF = A[F,:][:,F]
            AFC = A[F,:][:,C]
            
        if isinstance(self.smoother, SmootherInterface):
            self.smoother.setup(AFF)
        
        return AFF, AFC, C, F

        
    def _apply(self, b, x0, setupdata):
        AFF, AFC, C, F = setupdata
        x0[F] = self.smoother(AFF, b[F] - AFC@x0[C], x0[F])
        return x0
    
class OptimalSmoothing(SmootherInterface):
    def __init__(self, Nmax=2000, alternative_smoother=ChebyshevSmoother(), save=True, iter=None):
        assert iter is None
        super().__init__(None, save)
        self.Nmax = Nmax
        self.alternative_smoother = alternative_smoother
        
    def _setup(self, A, C, *args, **kwargs):
        if A.shape[1] > self.Nmax:
            if isinstance(self.alternative_smoother, SmootherInterface):
                self.alternative_smoother.setup(A)
            return A
        else:
            if kron.iskronrelated(A):
                A = A.toarray()
                C = C.toarray().reshape(-1)
            F = np.bitwise_not(C)
            AFF = A[F,:][:,F]
            AFC = A[F,:][:,C]
            return AFF, AFC, C, F
    
    def _apply(self, b, x0, setupdata):
        if isinstance(setupdata, tuple):
            AFF, AFC, C, F = setupdata
            x0[F] = np.linalg.solve(AFF, b[F] - AFC@x0[C])
            return x0
        else:
            A = setupdata
            return self.alternative_smoother(A,b,x0)        
        
class CompositSmoother(SmootherInterface):
    
    def __init__(self, smoothers, save=True, iter=None):
        assert iter is None
        super().__init__(None, save)
        self.smoothers = smoothers
        
    def _setup(self, A, *args, **kwargs):
        for S in self.smoothers:
            if hasattr(S,'setup'):
                S.setup(A, *args, **kwargs)
        return A
            
    def _apply(self, b, x0, setupdata):
        A = setupdata
        for S in self.smoothers:
            x0 = S(A,b,x0)
        return x0
    
    def copy(self):
        dict_ = {k:v.copy() if hasattr(v,'copy') else v for k,v in self.__dict__.items() if not k.startswith('_')}
        dict_['smoothers'] = [S.copy() if hasattr(S,'copy') else S for S in self.smoothers]
        return self.__class__(**dict_)