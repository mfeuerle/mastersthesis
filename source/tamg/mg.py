import scipy
from scipy.sparse.linalg import LinearOperator, spsolve
from scipy.sparse import issparse, eye
import numpy as np
import numbers

from kron import iskronrelated, isblock, block
from kron.utils import tosparray

from .smoother import SmootherInterface, ChebyshevSmoother, DirectSolver

class MultiGrid:
    
    def __init__(self, A, P=None, R='P'):
        if P is None:
            numlayers = 1
            A = [A]
            P = []
            R = []
        else:
            numlayers = len(P)+1
            if not issparse(R) and R == 'P':
                R = [p.T for p in P]
            assert numlayers == len(R)+1
            
            if not isinstance(A, (list,tuple)): # A seems to be a single matrix
                A_ = [A]
                for l in range(numlayers-1):
                    
                    if isblock(A_[-1]) and isblock(R[l]) and isblock(P[l]):
                        blocks = np.full((R[l].bshape[0],P[l].bshape[1]), fill_value=0, dtype='object')
                        for p in range(A_[-1].bshape[0]):
                            for i in range(R[l].bshape[0]):
                                if abs(R[l]._blocks[i,p]).sum() == 0: continue
                                for k in range(A_[-1].bshape[1]):
                                    for j in range(P[l].bshape[1]):
                                        if abs(P[l]._blocks[k,j]).sum() == 0: continue
                                        blocks[i,j] += R[l]._blocks[i,p] @ A_[-1]._blocks[p,k] @ P[l]._blocks[k,j]
                        A_.append(block(blocks))
                    else:  
                        A_.append(R[l] @ A_[-1] @ P[l])
                        
                A = A_
            assert numlayers == len(A)
            assert A[-1].shape[0] > 1
        
        for i in range(len(P)):
            assert A[i].shape[0] == R[i].shape[1]
            assert A[i].shape[1] == P[i].shape[0]
            assert A[i+1].shape[0] == R[i].shape[0]
            assert A[i+1].shape[1] == P[i].shape[1]
        
        self.numlayers = numlayers
        self.A = A
        self.R = R
        self.P = P
        
    @property
    def coarsest_idx(self):
        return self.numlayers-1
        
    def __len__(self):
        return self.numlayers
        
    def iscoarsest(self,l):
        return l==self.coarsest_idx or (l==-1 and self.numlayers==1)
    
    def get_layer(self,l):
        assert l < self.numlayers
        if self.iscoarsest(l):
            return self.get_coarsest()
        else:
            return {'A':self.A[l], 'R':self.R[l], 'P':self.P[l]}        
        
    def get_coarsest(self):
        return {'A' : self.A[self.coarsest_idx]}
    
    def add_layer(self, P, A=None, R='P'):
        if isinstance(P, dict):
            assert A is None and R=='P'
            data = P
            P = data['P']
            R = data['R']
            A = data['A']
        if not issparse(R) and R == 'P':
            R = P.T
        if A is None:
            A = R @ self.A[-1] @ P
        
        assert self.A[-1].shape[0] == R.shape[1]
        assert self.A[-1].shape[1] == P.shape[0]
        assert A.shape[0] == R.shape[0]
        assert A.shape[1] == P.shape[1]
        
        self.numlayers += 1
        self.A.append(A)
        self.P.append(P)
        self.R.append(R)
        
    _add_layer = add_layer
        
    def add_layers(self, P, A=None, R='P'):
        if all(isinstance(p,dict) for p in P):
            assert A is None and R=='P'
            for data in P:
                self._add_layer(data)
                      
        if not issparse(R) and R == 'P':
            R = [p.T for p in P]
        assert len(P) == len(R)
        
        if A is None:
            A_ = self.A[-1]
            A = []
            for l in range(len(P)):
                A_ = R[l] @ A_[-1] @ P[l]
                A.append(A_)
        assert len(P) == len(A)
        
        for i in range(len(P)):
            self._add_layer(P[i],A[i],R[i])
    
    def _transpose(self):
        return MultiGrid(A=[A.T for A in self.A], P=[R.T for R in self.R], R=[P.T for P in self.P])
    
    def layer_sizes(self):
        return [A.shape for A in self.A]
    
    def __repr__(self):
        L = self.numlayers
        M1,N1 = self.A[0].shape
        M2,N2 = self.A[-1].shape
        return '<%s with %s layer(s) ranging from %sx%s to %sx%s>' % (self.__class__.__name__, L, M1,N1, M2,N2)
    
    
# restructure as
# MultiGridOptions
# MultigridOperator(MultiGrid, MultiGridOptions)
# MultigridPreconditioner(MultiGrid, MultiGridOptions)    

# Oder: gemeinsame baseclass für beide.
# Aufrufen mit MultigridOperator(MultiGrid, opt1, op2, op3,...)
# Aufrufen mit MultigridPreconditioner(MultiGrid, opt1, op2, op3,...)
# anschließend .asoperator() und .asprecon() mit einigen wenigen optionen wie 'x0' beim switch 
class MultiGridOperator:
    
    def __init__(self, grid : MultiGrid, pre_smoother=ChebyshevSmoother(), post_smoother='pre', cycle_type='F', omega=1, coarsest_solver=DirectSolver()):
        self.grid = grid
        
        if pre_smoother is None:
            pre_smoother = grid.numlayers-1 * [lambda A,b,x0: x0]
        else:
            if hasattr(pre_smoother, '__len__'):
                assert len(pre_smoother) == grid.numlayers-1
            else:
                if isinstance(pre_smoother, SmootherInterface):
                    pre_smoother = [pre_smoother.copy() for i in range(grid.numlayers-1)]
                else:
                    pre_smoother = (grid.numlayers-1) * [pre_smoother]
            for i in range(grid.numlayers-1):
                if isinstance(pre_smoother[i], SmootherInterface):
                    pre_smoother[i].setup(**grid.get_layer(i))
        self.pre_smoother = pre_smoother
            
        if post_smoother == 'pre':
            post_smoother = pre_smoother
        elif post_smoother is None:
            post_smoother = (grid.numlayers-1) * [lambda A,b,x0: x0]
        else:
            if hasattr(post_smoother, '__len__'):
                assert len(post_smoother) == grid.numlayers-1
            else:
                if isinstance(post_smoother, SmootherInterface):
                    post_smoother = [post_smoother.copy() for i in range(grid.numlayers-1)]
                else:
                    post_smoother = (grid.numlayers-1) * [post_smoother]
            for i in range(grid.numlayers-1):
                if isinstance(post_smoother[i], SmootherInterface):
                    post_smoother[i].setup(**grid.get_layer(i))
        self.post_smoother = post_smoother
            
        self.cycle = self._get_cycle(cycle_type)
        self.omega = omega
        if isinstance(coarsest_solver, SmootherInterface):
            coarsest_solver.setup(grid.get_coarsest()['A'])
        self._solve_coarsest = coarsest_solver
            
    
    def get_initial_value(self, b, mode='N'):
        inverse_cycle = self._get_inverse_gamma_cycle(mode)
        return inverse_cycle(b)
    
    def apply_one_iteration(self, b, x0):
        return self.cycle(b, x0, 0)
        
    
    def aspreconditioner(self, x0_mode=None):
        return MultiGridPreconditioner(self, x0_mode)           
          
        
    def _get_cycle(self, cycle_type):
        if isinstance(cycle_type, numbers.Number):
            assert isinstance(cycle_type, int) and cycle_type >= 0
            gamma = cycle_type
            return lambda f,u,lvl: self._gamma_cycle(f, u, lvl, gamma)
        elif isinstance(cycle_type, str):
            return {'F' : self._F_cycle,
                    'V' : self._V_cycle,
                    'W' : self._W_cycle}[cycle_type]
        else:
            raise ValueError("Unknown cycle type")
        
    def _get_inverse_gamma_cycle(self, cycle_type):
        if isinstance(cycle_type, numbers.Number):
            assert isinstance(cycle_type, int) and cycle_type >= -1
            gamma = cycle_type
            return lambda f: self._inverse_gamma_cycle(f, gamma)
        elif isinstance(cycle_type, str):
            return {'N' : self._inverse_N_cycle,
                    'F' : self._inverse_F_cycle,
                    'V' : self._inverse_V_cycle,
                    'W' : self._inverse_W_cycle}[cycle_type]
        else:
            raise ValueError("Unknown inverse cycle type")
    
        
    def _gamma_cycle(self, f, u, lvl, gamma):
        if self.grid.iscoarsest(lvl):
            A = self.grid.get_coarsest()['A']
            return self._solve_coarsest(A,f,u)
        
        layer = self.grid.get_layer(lvl)
        A,R,P = [layer.get(k) for k in ['A','R','P']]
        
        u = self.pre_smoother[lvl](A,f,u)
        r = f - A @ u
        r = R @ r
        d = np.zeros([P.shape[1]] + [i for i in u.shape[1:]])
        
        if not self.grid.iscoarsest(lvl+1):
            if gamma == 0:
                d = self._F_cycle(r,d,lvl+1)
                d = self._V_cycle(r,d,lvl+1)
            else:
                for i in range(gamma):
                    d = self._gamma_cycle(r,d,lvl+1,gamma)
        else:
            d = self._gamma_cycle(r,d,lvl+1,gamma)  # call the coarse-grid solver only once
        
        d = P @ d
        u = u + self.omega * d
        u = self.post_smoother[lvl](A,f,u)
        return u
        
    def _F_cycle(self, f, u, lvl):
        return self._gamma_cycle(f, u, lvl, 0)
    
    def _V_cycle(self, f, u, lvl):
        return self._gamma_cycle(f, u, lvl, 1)
    
    def _W_cycle(self, f, u, lvl):
        return self._gamma_cycle(f, u, lvl, 2)
    
    
    
    def _inverse_gamma_cycle(self, f, gamma):
        f = [f]
        for lvl in range(self.grid.numlayers-1):
            layer = self.grid.get_layer(lvl)
            A,R,P = [layer.get(k) for k in ['A','R','P']]
            f.append(R @ f[-1])
        
        A = self.grid.get_coarsest()['A']
        u = self._solve_coarsest(A,f[-1],None)
            
        if gamma == -1:
            for lvl in range(self.grid.numlayers-2,-1,-1):
                layer = self.grid.get_layer(lvl)
                A,R,P = [layer.get(k) for k in ['A','R','P']]
                u = P @ u
                u = self.post_smoother[lvl](f[lvl],u)
        else:
            for lvl in range(self.grid.numlayers-2,-1,-1):
                layer = self.grid.get_layer(lvl)
                A,R,P = [layer.get(k) for k in ['A','R','P']]
                u = P @ u
                u = self.post_smoother[lvl](f[lvl],u)
                r = f[lvl] - A @ u
                d = self._gamma_cycle(r, np.zeros([A.shape[1]] + [i for i in u.shape[1:]]), lvl, gamma)
                u = u + self.omega * d
        return u
    
    def _inverse_N_cycle(self, f):
        return self._inverse_gamma_cycle(f, -1)
    
    def _inverse_F_cycle(self, f):
        return self._inverse_gamma_cycle(f, 0)
    
    def _inverse_V_cycle(self, f):
        return self._inverse_gamma_cycle(f, 1)
    
    def _inverse_W_cycle(self, f):
        return self._inverse_gamma_cycle(f, 2)
    
    

class MultiGridPreconditioner(LinearOperator):
    
    def __init__(self, MG : MultiGridOperator, x0_init='zeros'):
        A = MG.grid.get_layer(0)['A']
        super().__init__(A.dtype, A.shape)
        self.MG = MG
        self._x0_init = x0_init
        if x0_init == 'zeros':
            self._get_x0 = lambda b: np.zeros([A.shape[1]] + [i for i in b.shape[1:]])
        elif isinstance(x0_init, (str,int)):
            self._get_x0 = self.MG._get_inverse_gamma_cycle(x0_init)
        else:
            self._get_x0 = self._x0_init
        self._T = None
        super()._init_dtype()
        
    def __matmul__(self, b):
        x0 = self._get_x0(b)
        return self.MG.apply_one_iteration(b,x0)
        
    _matvec = __matmul__
    _matmat = __matmul__
    
    def _adjoint(self):
        if self._T is None:
            from copy import copy
            MGT = copy(self.MG)
            MGT.grid = MGT.grid._transpose()
            self._T = MultiGridPreconditioner(MGT, x0_init=self._x0_init)
        return self._T
    
    
def mgsolve(A : MultiGridOperator, b, x0='V', tol=1e-5, maxiter=None, atol=0, callback=None):
        
    if isinstance(x0, (str,int)):
        x = A.get_initial_value(b, x0)
    else:
        x = x0
        
    if maxiter is None:
        maxiter = 10*x.shape[0]
    if callback is None:
        callback = lambda x: None
    
    TOL = np.max([tol*np.linalg.norm(b), atol])
    A_finest = A.grid.get_layer(0)['A']
    for k in range(maxiter):
        x = A.apply_one_iteration(b,x)
        callback(x)
        if np.linalg.norm(b - A_finest @ x) < TOL:
            return x, 0
    return x, k
    
    
def cg(A, b, x0=None, tol=1e-5, maxiter=None, M=None, atol=0, callback=None):
    if x0 is None:
        x = np.random.rand(A.shape[1])
    else:
        x = x0
        
    if maxiter is None:
        maxiter = 10*x.shape[0]
        
    if M is None:
        class dummy:
            def __matmul__(self,other):
                return other
        M = dummy()
        
    if callback is None:
        callback = lambda x: None
        
    TOL = np.max([tol*np.linalg.norm(b), atol])
    
    r = b - A @ x
    d = M @ r
    rho0 = np.sum(d*r, axis=0)
    for k in range(maxiter):
        if np.linalg.norm(r) < TOL: 
            return x, 0
        a = A @ d
        alpha = rho0 / np.sum(d*a, axis=0)
        x = x + alpha*d        
        r = r - alpha*a        
        q = M @ r
        rho1 = np.sum(q*r, axis=0)
        d = q + rho1/rho0*d
        rho0 = rho1
        
        callback(x)
    return x, k


def mgpcgsolve(A : MultiGridOperator, b, x0='V', tol=1e-5, maxiter=None, M='MultiGrid', atol=0, callback=None):
    
    if isinstance(x0, (str,int)):
        x0 = A.get_initial_value(b, x0)
        
    if M == 'MultiGrid':
        M = MultiGridPreconditioner(A)
        
    A = A.grid.get_layer(0)['A']
    
    return cg(A, b, x0, tol, maxiter, M, atol, callback)


