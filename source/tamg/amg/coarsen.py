import copy
import numpy as np
from kron import vstack, block, isblock, iskronblock
from scipy.sparse import issparse    
    
class StandardCoarsening:
    # better Name?
    # Figure 11 in Stüben
    
    def __init__(self, scaling=2):
        self.scaling = scaling
    
    def __call__(self, S):                
        assert S.shape[0] == S.shape[1]
        
        cfsplit = np.full(shape=(S.shape[1],), fill_value='U')
        
        U = np.ones(shape=(S.shape[1],), dtype=bool)
        
        lmbda = np.zeros(shape=(S.shape[1],))        
        lmbda = S.sum(axis=0)
        
        while True:
            i = lmbda.argmax()
            if lmbda[i] == 0: break
            
            # add node to coarse
            cfsplit[i] = 'C'
            U[i]  = False            
            
            # add its neighbors to fine
            undecided_neighbors = (S[:,[i]].toarray().reshape(-1) if issparse(S) else S[:,i]).astype(bool) & U
            cfsplit[undecided_neighbors] = 'F'
            U[undecided_neighbors] = False
            
            # update lmbda
            s = S[[i],:].toarray().reshape(-1) if issparse(S) else S[i,:]
            idx = s.astype(bool) & U
            lmbda[idx] -= 1 * s[idx]
            for s in S[undecided_neighbors,:]:
                s = s.toarray().reshape(-1) if issparse(s) else s
                idx = s.astype(bool) & U
                lmbda[idx] += (self.scaling - 1) * s[idx]
            lmbda[i] = 0
            lmbda[undecided_neighbors] = 0
        
        cfsplit[U] = 'F'
        
        return cfsplit
    
    def __repr__(self) -> str:
        return '<%s(%s)>' % (self.__class__.__name__, self.scaling)
    
    
class MatrixCollectionCoarsening:
    
    def __init__(self, coarsening=StandardCoarsening()):
        self.coarsening = coarsening
        
    def __call__(self, S):
        Scom,S = S
        cfsplit = self.coarsening(Scom)
        return cfsplit
    
    def __repr__(self) -> str:
        return '<%s(%s)>' % (self.__class__.__name__, self.coarsening)
    
    
class BlockMatrixCoarsening:
    
    def __init__(self, coarsening=StandardCoarsening()):
        self.coarsening = coarsening
        
    def __call__(self, S):
        assert S.shape[0] == S.shape[1]
        assert isblock(S) and not iskronblock(S)
        M,N = S.bshape
        
        S_full = S.assemble(ifsparse='csr')
        # cfsplit based on the whole matrix
        cfsplit = self.coarsening(S_full)
        
        # if subblock has no C at all add one
        F = cfsplit=='F'
        C = cfsplit=='C'
        # for i in range(M):
        #     i_idx = slice(S._offsets[0][i], S._offsets[0][i+1])
        #     if sum(cfsplit[i_idx] == 'C') == 0:
        #         nS = S_full[F,:][:,i_idx].sum(axis=0)
        #         idx = np.argmax(nS)
        #         if nS[idx] == 0:
        #             nS = S_full[i_idx,:][:,C].sum(axis=1)
        #             idx = np.argmin(nS)
        #         cfsplit[S._offsets[0][i]+idx] = 'C'
        for j in range(N):
            j_idx = slice(S._offsets[1][j], S._offsets[1][j+1])
            if sum(cfsplit[j_idx] == 'C') == 0:
                nS = S_full[F,:][:,j_idx].sum(axis=0)
                idx = np.argmax(nS)
                if nS[idx] == 0:
                    nS = S_full[j_idx,:][:,C].sum(axis=1)
                    idx = np.argmin(nS)
                cfsplit[S._offsets[1][j]+idx] = 'C'
        return block(cfsplit, sbshape=(S.sbshape[1],1))
                
    def __repr__(self) -> str:
        return '<%s(%s)>' % (self.__class__.__name__, self.coarsening)
            
            
class BlockMatrixCollectionCoarsening:
    
    def __init__(self, ccoarsening=MatrixCollectionCoarsening(), bcoarsening=BlockMatrixCoarsening()):
        self.ccoarsening = ccoarsening
        self.bcoarsening = bcoarsening
        
    def __call__(self, S):
        coarsening = copy.copy(self.ccoarsening)
        coarsening.coarsening = self.bcoarsening
        return coarsening(S)
    
    def __repr__(self) -> str:
        return '<%s(%s,%s)>' % (self.__class__.__name__, self.ccoarsening, self.bcoarsening)
        

class MixedBlockMatrixCoarsening:
    pass        