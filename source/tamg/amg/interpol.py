import copy
import numpy as np
import scipy.sparse.linalg
from scipy.sparse import issparse, find, diags, csr_array, coo_array, vstack
import warnings

import kron
from kron.utils import tosparray, MatrixCollection, iscollection


class _InterpolationBase:
    
    def __call__(self, A, S, cfsplit):
        if cfsplit.ndim == 2:
            cfsplit = cfsplit.reshape(-1)
        C = cfsplit == 'C'
        F = cfsplit == 'F'
        if sum(F) == 0:
            assert len(C) == sum(C) == A.shape[1]
            return tosparray(scipy.sparse.eye(len(C), format='csr')) if issparse(A) else np.eye(len(C))
        PF = self._F_interpolation(A,S,C,F)
        if issparse(A):
            row,col,data = find(PF)
            C_idx = C.nonzero()[0]
            F_idx = F.nonzero()[0]
            row = np.append(F_idx[row], C_idx)
            col = np.append(col, range(len(C_idx)))
            data = np.append(data, np.ones((len(C_idx),)))
            P = csr_array((data, (row,col)), shape=(A.shape[1], PF.shape[1]), copy=False)      
        else:
            P = np.zeros((A.shape[1], PF.shape[1]))
            P[F] = PF
            P[C] = np.eye(sum(C))
        return P
    
    def _F_interpolation(self,A,S,C,F):
        raise NotImplementedError()


class OptimalInterpolation(_InterpolationBase):
    
    def __init__(self, eps_truncate=0):
        self.eps_truncate = eps_truncate
    
    def _F_interpolation(self,A,S,C,F):        
        AFF = A[F,:][:,F]
        AFC = A[F,:][:,C]
        
        if issparse(A):
            PF = -scipy.sparse.linalg.spsolve(AFF.tocsc(),AFC.tocsc())
        else:
            PF = -np.linalg.solve(AFF,AFC)
            
        if self.eps_truncate:
            # truncation
            s_old = abs(PF).sum(axis=1)
            if issparse(PF):
                PF = PF.tocsr()
                for i in range(PF.shape[0]):
                    idx = PF.indices[PF.indptr[i]:PF.indptr[i+1]]
                    PF.data[idx][ abs(PF.data[idx]) < self.eps_truncate * abs(PF.data[idx]).max() ] = 0
                PF.eliminate_zeros()
            else:
                for i in range(PF.shape[0]):
                    PF[i][ abs(PF[i]) < self.eps_truncate * abs(PF[i]).max() ] = 0
            # re-scaling
            s_new = abs(PF).sum(axis=1)
            s_new[s_old==0] = 1
            PF = PF * (s_old/s_new).reshape((-1,1))
                
        return PF
    
    def __repr__(self) -> str:
        return '<%s(%s)>' % (self.__class__.__name__, self.eps_truncate)

class DirectInterpolation(_InterpolationBase):
    
    def __init__(self, scaling='universal', sparse=True):
        assert scaling in ('universal', 'rowsum', 'diag')
        self.scaling = scaling
        self.sparse = sparse
        
    def __repr__(self) -> str:
        return '<%s(%s,%s)>' % (self.__class__.__name__, self.scaling,self.sparse)
        
    def _F_interpolation(self,A,S,C,F):
        assert A.shape[0] == A.shape[1]
        
        if self.scaling == 'universal':    
            PF = self._F_interpolation_universal(A,S,C,F)  
        elif self.scaling == 'rowsum':
            PF = self._F_interpolation_rowsum(A,S,C,F)
        elif self.scaling == 'diag':
            PF = self._F_interpolation_diag(A,S,C,F)
        else:
            raise ValueError(f"scaling = {self.scaling} not knwon")
        
        return PF   
    
    
    def _F_interpolation_universal(self,A,S,C,F):
        # mix zwischen Stüben und Xu-Zikatanov
        # Positive und negative Werte im Betrag betrachten, skalieren auf Zeilensumme = 1
        # Funktioniert für Diagonalen = 0 und Zeilensummen = 0
        # im Grunde identisch mit rowsum, jedoch matrix im Betrag
        # funktioniert erstaunlich gut, z.B. diag(1,4,-2) hat für beide alternativen als auch Optimal wechselnde vorzeichen,
        # in der Variante nicht, funktioniert trotzdem. Obwohl VZ nicht ganz egal, da für -PF gehts nicht
        AF = abs(A[F])
        SF = S[F]
        
        if not self.sparse:
            interpol_nodes  = C
            interpol_nodes_ = np.array(range(sum(C)))
            
        if issparse(A):
            data = AF.shape[0] * [[]]
            indices = AF.shape[0] * [[]]
            indptr = np.zeros((AF.shape[0]+1,), dtype=int)
            for i in range(AF.shape[0]):
                if self.sparse:
                    interpol_nodes  = SF[[i]].toarray().reshape(-1) & C if issparse(S) else SF[i] & C
                    interpol_nodes_ = SF[[i],C].nonzero()[0]
                  
                indptr[i+1] = indptr[i]
                
                s = AF[[i],interpol_nodes].sum()
                if s > 0 and len(interpol_nodes_) != 0:
                    data[i] = AF[[i],interpol_nodes] / s
                    idx = data[i].nonzero()[0]
                    data[i] = data[i][idx]
                    indices[i] = interpol_nodes_[idx]
                    indptr[i+1] += len(data[i])
            data = np.concatenate(data)
            indices = np.concatenate(indices)
            PF = csr_array((data, indices, indptr), shape=(AF.shape[0],sum(C)), copy=False)
            
        else:
            PF = np.zeros((AF.shape[0],sum(C)))
            for i in range(AF.shape[0]):
                if self.sparse:
                    interpol_nodes  = SF[[i]].toarray().reshape(-1) & C if issparse(S) else SF[i] & C
                    interpol_nodes_ = SF[[i],C]
                s = AF[i,interpol_nodes].sum()
                if s > 0:
                    PF[i,interpol_nodes_] = AF[i,interpol_nodes] / s    
        return PF
    
    def _F_interpolation_rowsum(self,A,S,C,F):
        # basic Xu-Zikatanov, 12.3.1. Prolongation "Direct Interpolation"
        # keine Unterscheidung zwischen positiven und negativen, skalieren auf Zeilensumme = 1
        # benötigt Zeilensumme != 0
        AF = A[F]
        SF = S[F]
        
        if not self.sparse:
            interpol_nodes  = C
            interpol_nodes_ = np.array(range(sum(C)))
            
        if issparse(A):
            data = AF.shape[0] * [[]]
            indices = AF.shape[0] * [[]]
            indptr = np.zeros((AF.shape[0]+1,), dtype=int)
            for i in range(AF.shape[0]):
                if self.sparse:
                    interpol_nodes  = SF[[i]].toarray().reshape(-1) & C if issparse(S) else SF[i] & C
                    interpol_nodes_ = SF[[i],C].nonzero()[0]
                    
                indptr[i+1] = indptr[i]
                
                s = AF[[i],interpol_nodes].sum()
                if s == 0 and len(interpol_nodes_) != 0:
                    if abs(AF[[i],interpol_nodes]).sum() == 0: continue
                    warnings.warn("dividing by 0 rowsum, use other scaling")
                data[i] = AF[[i],interpol_nodes] / s
                idx = data[i].nonzero()[0]
                data[i] = data[i][idx]
                indices[i] = interpol_nodes_[idx]
                indptr[i+1] += len(data[i])
            data = np.concatenate(data)
            indices = np.concatenate(indices)
            PF = csr_array((data, indices, indptr), shape=(AF.shape[0],sum(C)), copy=False)
            
        else:
            PF = np.zeros((AF.shape[0],sum(C)))
            for i in range(AF.shape[0]):
                if self.sparse:
                    interpol_nodes  = SF[[i]].toarray().reshape(-1) & C if issparse(S) else SF[i] & C
                    interpol_nodes_ = SF[[i],C]
                s = AF[i,interpol_nodes].sum()
                if s == 0 and len(interpol_nodes_) != 0:
                    if abs(AF[i,interpol_nodes]).sum() == 0: continue
                    warnings.warn("dividing by 0 rowsum, use other scaling")
                PF[i,interpol_nodes_] = AF[i,interpol_nodes] / s
        return PF
    
    
    def _F_interpolation_diag(self,A,S,C,F):
        # basic Stüben (82) und (120)
        # Positive und negative Werte getrennt betrachten, skalieren mit Diagonale
        # benötigt diagonale != 0
        diag_A = A.diagonal()
        AF = ( A - tosparray(diags(diag_A)) )[F]
        diag_A = diag_A[F]
        SF = S[F]
        
        if not self.sparse:
            interpol_nodes  = C
            interpol_nodes_ = np.array(range(sum(C)))
            
        if issparse(A):
            data = AF.shape[0] * [[],[]]
            indices = AF.shape[0] * [[],[]]
            indptr = np.zeros((AF.shape[0]+1,), dtype=int)
            for i in range(AF.shape[0]):
                if self.sparse:
                    interpol_nodes  = SF[[i]].toarray().reshape(-1) & C if issparse(S) else SF[i] & C
                    interpol_nodes_ = SF[[i],C].nonzero()[0]
                AFi = AF[[i]].toarray().reshape(-1)
                
                indptr[i+1] = indptr[i]
                
                AFi_neg = 0.5 * (AFi - abs(AFi))
                s_neg = AFi_neg[interpol_nodes].sum()
                if s_neg < 0 and len(interpol_nodes_) != 0:
                    if diag_A[i] == 0: warnings.warn("dividing by 0 diagonal, use other scaling")
                    data[2*i] = -(AFi_neg.sum() / s_neg) * AFi_neg[interpol_nodes]/diag_A[i]
                    idx = data[2*i].nonzero()[0]
                    data[2*i] = data[2*i][idx]
                    indices[2*i] = interpol_nodes_[idx]
                    indptr[i+1] += len(data[2*i])
                AFi_pos = 0.5 * (AFi + abs(AFi))
                s_pos = AFi_pos[interpol_nodes].sum()
                if s_pos > 0 and len(interpol_nodes_) != 0:
                    if diag_A[i] == 0: warnings.warn("dividing by 0 diagonal, use other scaling")
                    data[2*i+1] = -(AFi_pos.sum() / s_pos) * AFi_pos[interpol_nodes]/diag_A[i]
                    idx = data[2*i+1].nonzero()[0]
                    data[2*i+1] = data[2*i+1][idx]
                    indices[2*i+1] = interpol_nodes_[idx]
                    indptr[i+1] += len(data[2*i+1])
            data = np.concatenate(data)
            indices = np.concatenate(indices)
            PF = csr_array((data, indices, indptr), shape=(AF.shape[0],sum(C)), copy=False)
            
        else:
            PF = np.zeros((AF.shape[0],sum(C)))
            for i in range(AF.shape[0]):
                if self.sparse:
                    interpol_nodes  = SF[[i]].toarray().reshape(-1) & C if issparse(S) else SF[i] & C
                    interpol_nodes_ = SF[[i],C]
                
                AFi_neg = 0.5 * (AF[i] - abs(AF[i]))
                s_neg = AFi_neg[interpol_nodes].sum()
                if s_neg < 0 and len(interpol_nodes_) != 0:
                    if diag_A[i] == 0: warnings.warn("dividing by 0 diagonal, use other scaling")
                    PF[i,interpol_nodes_] -= (AFi_neg.sum() / s_neg) * AFi_neg[interpol_nodes]/diag_A[i]
                AFi_pos = 0.5 * (AF[i] + abs(AF[i]))
                s_pos = AFi_pos[interpol_nodes].sum()
                if s_pos > 0 and len(interpol_nodes_) != 0:
                    if diag_A[i] == 0: warnings.warn("dividing by 0 diagonal, use other scaling")
                    PF[i,interpol_nodes_] -= (AFi_pos.sum() / s_pos) * AFi_pos[interpol_nodes]/diag_A[i]
        return PF
    
    
class StandardInterpolation(_InterpolationBase):
    
    def __init__(self, scaling='universal', eps_truncate=0.2, sparse=True):
        assert scaling in ('universal', 'rowsum')
        self.scaling = scaling
        self.eps_truncate = eps_truncate
        self.sparse = sparse
        
    def __repr__(self) -> str:
        return '<%s(%s,%s,%s)>' % (self.__class__.__name__, self.scaling,self.eps_truncate,self.sparse)
        
    def _F_interpolation(self,A,S,C,F):
        assert A.shape[0] == A.shape[1]
        
        if self.scaling == 'universal':
            PF = self._F_interpolation_universal(A,S,C,F)
        elif self.scaling == 'rowsum':
            PF = self._F_interpolation_rowsum(A,S,C,F)
        else:
            raise ValueError(f"scaling = {self.scaling} not knwon")
        
        return PF
    
    
    def _F_interpolation_universal(self,A,S,C,F):
        return UniversalInterpolation(order=1, eps_truncate=self.eps_truncate, sparse=self.sparse)._F_interpolation(A,S,C,F)
    
    
    def _F_interpolation_rowsum(self,A,S,C,F):
        # Xu-Zikatanov (12.16)
        AF = A[F]
        SF = S[F]
        diag_A = A.diagonal()[F]
        sC = sum(C)
        F_idx = F.nonzero()[0]
        
        if not self.sparse:
            interpol_nodes  = C
            interpol_nodes_ = np.array(range(sC))
            F_nodes_ = range(sum(F))
            
        if issparse(A):
            p = AF.shape[0] * [[]]
            for i in range(AF.shape[0]):                
                if self.sparse:
                    interpol_nodes  = SF[[i]].toarray().reshape(-1) & C if issparse(S) else SF[i] & C
                    interpol_nodes_ = SF[[i],C].nonzero()[0]
                    F_nodes_ = SF[[i],F].nonzero()[0]                         
                
                # direct interpolation
                if len(interpol_nodes_) > 0:
                    data = [-AF[[i],interpol_nodes]]
                    col  = [interpol_nodes_]
                else:
                    data = []
                    col = []
                # indirect interpolation
                for j in F_nodes_:
                    if self.sparse:
                        interpol_nodes  = SF[[j]].toarray().reshape(-1) & C if issparse(S) else SF[j] & C
                        interpol_nodes_ = SF[[j],C].nonzero()[0]
                    
                    if len(interpol_nodes_) > 0:
                        dat = AF[i,F_idx[j]] * AF[[j],interpol_nodes]
                        if diag_A[j] == 0:
                            if abs(dat).sum(axis=1) == 0: continue
                            warnings.warn("dividing by 0 diagonal, use other scaling")
                        data += [1/diag_A[j] * dat]
                        col  += [interpol_nodes_]
                
                # summation of duplicate entries 
                data = np.concatenate(data)
                col  = np.concatenate(col)
                p[i] = coo_array((data, (len(data)*[0],col)), shape=(1,sC), copy=False)
                p[i].sum_duplicates()
                
                # truncation
                if self.eps_truncate != 0:
                    p[i].data[ abs(p[i].data) < self.eps_truncate * abs(p[i].data).max() ] = 0
                    p[i].eliminate_zeros()
                
                # scaling
                s = p[i].data.sum()
                if s == 0:
                    if abs(p[i]).sum() == 0: continue
                    warnings.warn("dividing by 0 rowsum, use other scaling")
                p[i].data = p[i].data / s
            
            PF = vstack(p)
            
        else:
            PF = np.zeros((AF.shape[0],sC))
            for i in range(AF.shape[0]):
                if self.sparse:
                    interpol_nodes  = SF[[i]].toarray().reshape(-1) & C if issparse(S) else SF[i] & C
                    interpol_nodes_ = SF[[i],C]
                    F_nodes_ = SF[[i],F].nonzero()[0]
                
                # direct interpolation    
                PF[i,interpol_nodes_] = -AF[i,interpol_nodes]
                # indirect interpolation
                for j in F_nodes_:
                    if self.sparse:
                        interpol_nodes  = SF[[j]].toarray().reshape(-1) & C if issparse(S) else SF[j] & C
                        interpol_nodes_ = SF[[j],C]
                    
                    dat = AF[i,F_idx[j]] * AF[j,interpol_nodes]
                    if diag_A[j] == 0:
                        if abs(dat).sum() == 0: continue
                        warnings.warn("dividing by 0 diagonal, use other scaling")
                    PF[i,interpol_nodes_] += dat/diag_A[j]
                
                # truncation
                if self.eps_truncate != 0:
                    PF[i][ abs(PF[i]) < self.eps_truncate * abs(PF[i]).max() ] = 0
                # scaling
                s = PF[i].sum()
                if s == 0:
                    if abs(PF[i]).sum() == 0: continue
                    warnings.warn("dividing by 0 rowsum, use other scaling")
                PF[i] = PF[i] / s
            
        return PF
    
    
class UniversalInterpolation(_InterpolationBase):
    
    def __init__(self, order=1, eps_truncate=0.2, sparse=True):
        self.order = order
        self.eps_truncate = eps_truncate
        self.sparse = sparse
        
    def __repr__(self) -> str:
        return '<%s(%s,%s,%s)>' % (self.__class__.__name__, self.order,self.eps_truncate,self.sparse)
        
        
    def _F_interpolation(self,A,S,C,F):
        assert A.shape[0] == A.shape[1]
        
        if self.order == 0:
            PF = DirectInterpolation(scaling='universal', sparse=self.sparse)._F_interpolation(A,S,C,F)
        else:
            PF_indirect = UniversalInterpolation(order=self.order-1, eps_truncate=0, sparse=self.sparse)._F_interpolation(A,S,C,F)
        
            AF = abs((A - tosparray(diags(A.diagonal())))[F])
            SF = S[F]
            sC = sum(C)
            F_idx = F.nonzero()[0]
            
            if not self.sparse:
                interpol_nodes  = C
                interpol_nodes_ = np.array(range(sC))
                CF_nodes = C|F
                F_nodes_ = range(sum(F))
                
            if issparse(A):
                p = AF.shape[0] * [[]]
                for i in range(AF.shape[0]):
                    if self.sparse:
                        interpol_nodes  = SF[[i]].toarray().reshape(-1) & C if issparse(S) else SF[i] & C
                        interpol_nodes_ = SF[[i],C].nonzero()[0]
                        CF_nodes = SF[[i]].toarray().reshape(-1) & (C|F) if issparse(S) else SF[i] & (C|F)
                        F_nodes_ = SF[[i],F].nonzero()[0]
                       
                    AFi = AF[[i]].toarray().reshape(-1)
                    AFi[np.invert(CF_nodes)] = 0
                    s = AFi.sum()
                    if s > 0 and len(interpol_nodes_) != 0:
                        # scaling
                        AFi = AFi / s
                        # direct interpolation
                        p[i] = csr_array((AFi[interpol_nodes],interpol_nodes_,[0,len(interpol_nodes_)]), shape=(1,sC), copy=False)
                        # indirect interpolation
                        for j in F_nodes_:
                            aij = AFi[F_idx[j]]
                            if aij != 0:
                                p[i] += aij * PF_indirect[[j]]
                        # truncation
                        if self.eps_truncate != 0:
                            p[i].data[ p[i].data < self.eps_truncate * p[i].data.max() ] = 0
                            p[i].eliminate_zeros()
                        # re-scaling
                        s2 = p[i].data.sum()
                        if s2 != 0:
                            p[i].data = p[i].data / s2
                    else:
                        p[i] = coo_array((1,sC))
                    
                PF = vstack(p)
                
            else:
                PF = np.zeros((AF.shape[0],sC))
                for i in range(AF.shape[0]):
                    if self.sparse:
                        interpol_nodes  = SF[[i]].toarray().reshape(-1) & C if issparse(S) else SF[i] & C
                        interpol_nodes_ = SF[[i],C]
                        CF_nodes = SF[[i]].toarray().reshape(-1) & (C|F) if issparse(S) else SF[i] & (C|F)
                        F_nodes_ = SF[[i],F].nonzero()[0]
                        
                    AFi = AF[i]
                    AFi[np.invert(CF_nodes)] = 0
                    s = AFi.sum()
                    if s > 0 and len(interpol_nodes_) != 0:
                        # scaling
                        AFi = AFi / s
                        # direct interpolation
                        PF[i,interpol_nodes_] = AFi[interpol_nodes]
                        # indirect interpolation
                        for j in F_nodes_:
                            aij = AFi[F_idx[j]]
                            if aij != 0:
                                PF[i] += aij * PF_indirect[j]
                        # truncation
                        if self.eps_truncate != 0:
                            PF[i, PF[i] < self.eps_truncate * PF[i].max()] = 0
                        # re-scaling
                        s2 = PF[i].sum()
                        if s2 != 0:
                            PF[i] = PF[i] / s2
                            
        return PF
    
    
class MatrixCollectionInterpolation:
    
    def __init__(self, interpolation=StandardInterpolation(), eps_truncate=0):
        self.interpolation = interpolation
        self.eps_truncate = eps_truncate
    
    def __call__(self, A, S, cfsplit):
        A = A[0] if len(A)==1 else MatrixCollection(*A)
        Scom,S = S
            
        assert iscollection(A)       
        assert iscollection(S)
        assert len(A) == len(S)
        
        # project each matrix with its own strongness
        P = MatrixCollection([self.interpolation(A[i],S[i],cfsplit) for i in range(len(A))])
        # combine all projections
        scale = sum( ( abs(Pi).sum(axis=1).toarray() if kron.isblock(Pi) else abs(Pi).sum(axis=1) ) > 0 for Pi in P)
        if scale.ndim == 1: 
            scale = scale.reshape((-1,1))
        P = sum(P) * (1/scale)
        
        # truncation
        if self.eps_truncate:
            if kron.isblock(P):
                assert not kron.iskronblock(P)
                sbshape = P.sbshape
                P = P.assemble(ifsparse='csr')
                
            s_old = P.sum(axis=1)
            if issparse(P):
                P = P.tocsr()
                for i in range(P.shape[0]):
                    idx = slice(P.indptr[i], P.indptr[i+1])
                    P.data[idx][ abs(P.data[idx]) < self.eps_truncate * abs(P.data[idx]).max() ] = 0
                P.eliminate_zeros()
            else:
                for i in range(P.shape[0]):
                    P[i][ abs(P[i]) < self.eps_truncate * abs(P[i]).max() ] = 0
            # re-scaling
            s_new = P.sum(axis=1)
            P = P * (s_old/s_new).reshape((-1,1))
            
            if kron.isblock(P):
                P = kron.block(P, sbshape=sbshape)
            
        return P
    
    
    def __repr__(self) -> str:
        return '<%s(%s,%s)>' % (self.__class__.__name__, self.interpolation,self.eps_truncate)
    
    
class BlockMatrixInterpolation:
    
    def __init__(self, interpolation=StandardInterpolation(), onlydiag=False):
        self.interpolation = interpolation
        self.onlydiag = onlydiag
        
    def __call__(self, A, S, cfsplit):
        if not kron.isblock(A):
            A = kron.block(A)
        assert kron.isblock(A) and not kron.iskronblock(A)
        assert A.sbshape == S.sbshape # falls R auch berechnet wäre nur bei onlydiag nötig
        
        M,N = A.bshape
        
        P = np.full((N,N), fill_value=None, dtype='object')
        
        if self.onlydiag:
            assert A.sbshape[0] == A.sbshape[1]
            for i in range(N):
                P[i,i] = self.interpolation(A._blocks[i,i], S._blocks[i,i], cfsplit._blocks[i,0])
            return kron.block(P)
                
        else:
            P_tmp = self.interpolation(A.assemble(ifsparse='csr'), S.assemble(ifsparse='csr'), cfsplit.toarray())
            return kron.block(P_tmp, sbshape=(A.sbshape[1],[sum(cfsplit._blocks[i,0][:,0]=='C') for i in range(N)]) )
    
    def __repr__(self) -> str:
        return '<%s(%s,%s)>' % (self.__class__.__name__, self.interpolation,self.onlydiag)
    
    
class BlockMatrixCollectionInterpolation:
    
    def __init__(self, cinterpolation=MatrixCollectionInterpolation(), binterpolation=BlockMatrixInterpolation()):
        self.cinterpolation = cinterpolation
        self.binterpolation = binterpolation
        self.onlydiag = binterpolation.onlydiag if hasattr(binterpolation, 'onlydiag') else False
        
    def __call__(self, A, S, cfsplit):
        assert kron.isblock(A) and not kron.isblock(S)
        assert not kron.iskronblock(A) and not kron.iskronblock(S) 
        assert A.iscollection() and iscollection(S[1])
        
        interpolation = copy.copy(self.cinterpolation)
        interpolation.interpolation = self.binterpolation
        return interpolation(A.ascollection(), S, cfsplit)
    
    def __repr__(self) -> str:
        return '<%s(%s,%s)>' % (self.__class__.__name__, self.cinterpolation, self.binterpolation)