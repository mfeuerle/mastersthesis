import copy
import numpy as np
from scipy.sparse import diags, issparse, csr_array, find
import kron
from kron.utils import tosparray, MatrixCollection, iscollection


            
class AnonymusStrength:
    
    def __init__(self, strength, eps_strong=0.25):
        assert 0 < eps_strong < 1
        self.eps_strong = eps_strong
        self.strength = strength
        
    def __call__(self, A):      
        if issparse(A): 
            I,J,AIJ = find(A)            
            S = np.zeros(shape=(len(AIJ),), dtype=int)
            nnz = 0
            # Parallisierung
            for k in range(len(AIJ)):
                if I[k] != J[k] and self.strength(AIJ[k],I[k],J[k]) >= self.eps_strong:
                    S[nnz] = k
                    nnz += 1
            S = S[:nnz]
            S = csr_array((np.ones(shape=(nnz,),dtype=bool), (I[S],J[S])), shape=A.shape, dtype=bool, copy=False)
            
        else:
            S = np.zeros(shape=A.shape, dtype=bool)
            for i in range(A.shape[0]):
                for j in range(A.shape[1]):
                    if i != j and self.strength(A[i,j],i,j) >= self.eps_strong:
                        S[i,j] = True
        
        return S
    
    def __repr__(self) -> str:
        return '<%s(%s)>' % (self.__class__.__name__, self.eps_strong)


class _RugeStubenBase:    
    def __init__(self, eps_strong=0.25):
        self.eps_strong = eps_strong
        
    def __call__(self, A):      
        A = A - tosparray(diags(A.diagonal()))
        
        denom = abs(A).max(axis=1) if self.abs else (-A).max(axis=1)
        if issparse(denom): denom = denom.toarray().reshape(-1)
        denom = np.maximum(denom,0)
        
        if issparse(A):
            if self.abs:
                def strength(aij,i,j): 
                    d = denom[[i,j]].min() if self.sym else denom[i]
                    return abs(aij)/d if d!=0 else -1
            else:
                def strength(aij,i,j): 
                    d = denom[[i,j]].min() if self.sym else denom[i]
                    return -aij/d if d!=0 else -1   
            S = AnonymusStrength(strength, eps_strong=self.eps_strong)(A)            
        else:
            S = np.zeros(shape=A.shape, dtype=bool)
            d = np.minimum(denom.reshape((-1,1)), denom.reshape((1,-1))) if self.sym else denom
            idx = d != 0
            S[idx] = ( ((abs(A) if self.abs else -A)[idx].T/ d[idx].T).T >= self.eps_strong )
        return S
    
    def __repr__(self) -> str:
        return '<%s(%s)>' % (self.__class__.__name__, self.eps_strong) 


class RugeStuben(_RugeStubenBase):
    # Stuben (115)
    # Xu-Zikatanov (8.2)
    # bei kron vorzeichen nicht eindeutig, deshalb ungeeignet
    abs = False
    sym = False
    
class RugeStubenSymmetric(_RugeStubenBase):
    # Xu-Zikatanov (8.7)
    # bei kron vorzeichen nicht eindeutig, deshalb ungeeignet
    abs = False
    sym = True

class RugeStubenAbs(_RugeStubenBase):
    # Stuben (115) aber mit absolut betrag
    # Xu-Zikatanov (8.2) aber mit absolut betrag
    abs = True
    sym = False
    
class RugeStubenAbsSymmetric(_RugeStubenBase):
    # Xu-Zikatanov (8.7) aber mit absolut betrag
    abs = True
    sym = True


class CauchySchwarz1:
    # Xu-Zikatanov (8.9)
    
    def __init__(self, eps_strong=0.25):
        self.eps_strong = eps_strong
        
    def __call__(self, A):
        D = A.diagonal()
        # unterscheidung für dense?
        def strength(aij,i,j): 
            d = np.sqrt(D[i]*D[j])
            return abs(aij)/d if d!=0 else -1
        return AnonymusStrength(strength, eps_strong=self.eps_strong)(A)
    
class CauchySchwarz2:
    # Xu-Zikatanov (8.10)
    # bei kron vorzeichen nicht eindeutig, deshalb ungeeignet
    
    def __init__(self, eps_strong=0.25):
        self.eps_strong = eps_strong
        
    def __call__(self, A):
        D = A.diagonal()
        # unterscheidung für dense?
        def strength(aij,i,j):
            d =  (D[i]+D[j])
            return -2*aij/d if d!=0 else -1
        return AnonymusStrength(strength, eps_strong=self.eps_strong)(A)
    
    
class MatrixCollectionStrength:
    #ncommon         = 'all' # (1,...,len(A),'all','sum', 'asum'), sum=S von der summe nehmen, 'all'=len(A), any=1
    
    def __init__(self, strength=RugeStubenAbs(), ncommon='all', weighted=True):
        self.strength = strength
        self.ncommon = 1 if ncommon=='any' else ncommon
        self.weighted = weighted
        
    def __call__(self, *A):
        A = A[0] if len(A)==1 else MatrixCollection(*A)
        assert iscollection(A)
            
        ncommon = len(A) if self.ncommon == 'all' else self.ncommon
        
        # individual strong connections
        S = MatrixCollection([self.strength(Ai) for Ai in A])
        
        # combined strong connections
        if ncommon == 'sum':
            Scom = self.strength(sum(A))
        elif ncommon == 'asum':
            Scom = self.strength(sum(abs(Ai) for Ai in A))
        else:
            if any(kron.isblock(Ai) for Ai in A):
                sbshape = A[0].sbshape
                assert all(kron.isblock(Si) and not kron.iskronblock(Si) and Si.sbshape == sbshape for Si in S)
                assert all(kron.isblock(Ai) and not kron.iskronblock(Ai) and Ai.sbshape == sbshape for Ai in A)
                
                Scom = sum(Si.assemble(ifsparse='csr').astype(int) for Si in S)
                nz = sum(abs(Ai.assemble(ifsparse='csr')).sum(axis=1) != abs(Ai.assemble(ifsparse='csr').diagonal()) for Ai in A)
            else:
                Scom = sum(Si.astype(int) for Si in S)
                nz = sum(abs(Ai).sum(axis=1) != abs(Ai.diagonal()) for Ai in A)
                
            if issparse(Scom):
                Scom = Scom.tocsr()
                Scom.eliminate_zeros()
                for i in range(Scom.shape[0]):
                    idx = slice(Scom.indptr[i], Scom.indptr[i+1])
                    Scom.data[idx] += len(A) - nz[i]
                    Scom.data[idx][Scom.data[idx] < ncommon] = 0
                Scom.eliminate_zeros()
                Scom.data += -ncommon + 1
            else:
                for i in range(Scom.shape[0]):
                    Scom[i,Scom[i] > 0] += len(A) - nz[i] - ncommon + 1
                Scom[Scom < 0] = 0
                       
            if not self.weighted:
                Scom = Scom.astype(bool)
                
            if kron.isblock(A[0]):
                Scom = kron.block(Scom, sbshape=sbshape)
             
        if issparse(Scom): Scom = Scom.tocsr()
        
        return Scom, S
    
    def __repr__(self) -> str:
        return '<%s(%s,%s,%s)>' % (self.__class__.__name__, self.strength, self.ncommon, self.weighted)
    
    
class BlockMatrixStrength:
    
    def __init__(self, strength=RugeStubenAbs(), separate=False):
        self.strength = strength
        self.separate = separate
        
    def __call__(self, A):
        assert kron.isblock(A) 
        assert not kron.iskronblock(A) 
        assert not A.hascollections()
        
        M,N = A.bshape
        
        S = np.full((M,N), fill_value=None, dtype='object')
        
        if self.separate:
            assert A.sbshape[0] == A.sbshape[1]
            for i in range(M):
                S[i,i] = self.strength(A._blocks[i,i])
            return kron.block(S)
        else:
            S = self.strength(A.assemble(ifsparse='csr'))
            return kron.block(S, sbshape=A.sbshape)
    
    def __repr__(self) -> str:
        return '<%s(%s,%s)>' % (self.__class__.__name__, self.strength, self.separate)
    

class BlockMatrixCollectionStrength:
    
    def __init__(self, cstrength=MatrixCollectionStrength(), bstrength=BlockMatrixStrength()):
        self.cstrength = cstrength
        self.bstrength = bstrength

        
    def __call__(self, A):
        assert kron.isblock(A) 
        assert not kron.iskronblock(A) 
        assert A.iscollection()

        strength = copy.copy(self.cstrength)
        strength.strength = self.bstrength
        return strength(A.ascollection())
    
    
    def __repr__(self) -> str:
        return '<%s(%s,%s)>' % (self.__class__.__name__, self.cstrength, self.bstrength)
        