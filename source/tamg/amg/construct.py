from configparser import Interpolation
from multiprocessing.sharedctypes import Value
import numpy as np
import scipy.sparse
from scipy.sparse import issparse
from scipy.sparse._sputils import isdense

import kron
from kron import iskronrelated, iskronshaped, iskron, iskronsum, isblock, iskronblock
from kron.utils import MatrixCollection, iscollection, tosparray

from tamg.mg import MultiGrid
from .strength import RugeStubenAbs, MatrixCollectionStrength, BlockMatrixStrength, BlockMatrixCollectionStrength
from .coarsen import StandardCoarsening, MatrixCollectionCoarsening, BlockMatrixCoarsening, BlockMatrixCollectionCoarsening
from .interpol import StandardInterpolation, MatrixCollectionInterpolation, BlockMatrixInterpolation, BlockMatrixCollectionInterpolation

    
class AlgebraicMultiGrid(MultiGrid):
        
    def __init__(self, A, C=None, P=None, R='P'):
        super().__init__(A,P,R)
        if C is None: 
            C = []
        assert len(C) == len(self.P)
        self.C = [c for c in C]        
        
    
    def get_layer(self,l):
        out = super().get_layer(l)
        if not self.iscoarsest(l):
            out['C'] = self.C[l]
        return out
    
    def add_layer(self, P, C=None, A=None, R='P'):
        if isinstance(P,dict):
            assert C is None and A is None and R=='P'
            data = P
            P = data['P']
            R = data['R']
            A = data['A']
            C = data['C']
        super().add_layer(P, A, R)
        self.C.append(C)
    
    def add_layers(self, P, C=None, A=None, R='P'):
        if all(isinstance(p,dict) for p in P):
            assert C is None and A is None and R=='P'
            for data in P: 
                self.add_layer(data)
        else:
            super().add_layers(P, A, R)
            self.C += [c for c in C]
            
    def tomultigrid(self):
        return MultiGrid(self.A, self.P, self.R)


class AMGFactoryInterface():
    
    Nmin : int
    todense = (0, 2/3)
    
    def __init__(self, *args, **kwargs):
        __doc__ = "Interface for AMG geneartions.\n" + self.options.__doc__
        
        self.options(*args, **kwargs)
    
    
        
    def options(self, Nmin=None, todense=None):
        f"""
        Parameters
        ----------
        Nmin
            minimal mareix size
        todense : False or (n,p)
            if not False, sparse layers with less then n unknowns or more then 100p% nonzero entries will converted to dense format.
            Example: (200, 0.6) -> matricies with less then 200 unknwons or more then 60% nonzero entries will be converted to dense format
        """
        if Nmin is not None:
            self.Nmin = Nmin
        if todense is not None:
            self.todense = todense
            
    def assemble_layer(self, A, *args, **kwargs):  
        __doc__ = self._assemble_layer.__doc__
             
        data = self._assemble_layer(A,*args,**kwargs)
        
        if not data:
            return False

        if data['C'].ndim==1:
            data['C'] = data['C'].reshape((-1,1))
        if not data['C'].dtype == bool:
            data['C'] = data['C'] == 'C'
        if 'R' not in data:
            data['R'] = data['P'].T
            
        if 'A' not in data:
            data['A'] = data['R'] @ A @ data['P']
            
        if self._below_Nmin(data['A']):
            return False

        if self.todense:
            data['A'] = self._todense(data['A'])
        
        return data    
    
    def _assemble_layer(self, A, *args, **kwargs):
        raise NotImplementedError("this is an interface, please implement this method")
    
    
    def _below_Nmin(self, A):
        return A.shape[1] < self.Nmin
    
    def _todense(self, A):
        if A.shape[1] <= self.todense[0]:
            A = A.toarray()
        elif not isdense(A) and A.nnz/(A.shape[0]*A.shape[1]) > self.todense[1]:
            A = A.toarray()
        return A
    
    
    def __repr__(self) -> str:
        s = f"<{self.__class__.__name__}("
        for k,v in self.__dict__.items():
            s += f"{k}={v},"
        return s[:-1]+'>'
    
    def __str__(self):
        s = f"{self.__class__.__name__}:\n"
        l = np.max([len(str(k)) for k,v in self.__dict__.items()])
        for k,v in self.__dict__.items():
            s += f"\t{k:>{l}s}: {v}\n"
        return s    
    
    
    
######################################################
# basic
######################################################
class AMGFactory(AMGFactoryInterface):
    strength        = 'auto'
    coarsening      = 'auto'
    interpolation   = 'auto'
    Nmin            = 10
    
    sbNmin = 5
    
    # default values
    dstrength      = RugeStubenAbs()
    dcoarsening    = StandardCoarsening()
    dinterpolation = StandardInterpolation()
    # block matrix values
    bstrength      = BlockMatrixStrength()
    bcoarsening    = BlockMatrixCoarsening()
    binterpolation = BlockMatrixInterpolation()
    # matrix collection values
    cstrength      = MatrixCollectionStrength()
    ccoarsening    = MatrixCollectionCoarsening()
    cinterpolation = MatrixCollectionInterpolation()
    # block matrix collection values
    bcstrength      = BlockMatrixCollectionStrength()
    bccoarsening    = BlockMatrixCollectionCoarsening()
    bcinterpolation = BlockMatrixCollectionInterpolation()
    
    def options(self, strength=None, coarsening=None, interpolation=None, Nmin=None, todense=None, sbNmin=None,
                dstrength=None, dcoarsening=None, dinterpolation=None,
                bstrength=None, bcoarsening=None, binterpolation=None,
                bcstrength=None, bccoarsening=None, bcinterpolation=None,
                cstrength=None, ccoarsening=None, cinterpolation=None):
        """
        """
        super().options(Nmin,todense)
        if sbNmin is not None:
            self.sbNmin = sbNmin
            
        if strength is not None:
            self.strength = strength
        if coarsening is not None:
            self.coarsening = coarsening
        if interpolation is not None:
            self.interpolation = interpolation
            
        if dstrength is not None:
            self.dstrength = dstrength
        if dcoarsening is not None:
            self.dcoarsening = dcoarsening
        if dinterpolation is not None:
            self.dinterpolation = dinterpolation
            
        if bstrength is not None:
            self.bstrength = bstrength
        if bcoarsening is not None:
            self.bcoarsening = bcoarsening
        if binterpolation is not None:
            self.binterpolation = binterpolation
            
        if bcstrength is not None:
            self.bcstrength = bcstrength
        if bccoarsening is not None:
            self.bccoarsening = bccoarsening
        if bcinterpolation is not None:
            self.bcinterpolation = bcinterpolation
            
        if cstrength is not None:
            self.cstrength = cstrength
        if ccoarsening is not None:
            self.ccoarsening = ccoarsening
        if cinterpolation is not None:
            self.cinterpolation = cinterpolation
            
    def default_matrix(self, strength=None, coarsening=None, interpolation=None):
        self.options(dstrength=strength, dcoarsening=coarsening, dinterpolation=interpolation)
        
    def block_matrix(self, strength=None, coarsening=None, interpolation=None):
        self.options(bstrength=strength, bcoarsening=coarsening, binterpolation=interpolation)
        
    def blockcollection_matrix(self, strength=None, coarsening=None, interpolation=None):
        self.options(bcstrength=strength, bccoarsening=coarsening, bcinterpolation=interpolation)
        
    def collection_matrix(self, strength=None, coarsening=None, interpolation=None):
        self.options(cstrength=strength, ccoarsening=coarsening, cinterpolation=interpolation)
            
    def _assemble_layer(self, A, b='all'):
        
        if isblock(A):
            if A.iscollection():
                strength      = self.bcstrength      if self.strength      == 'auto' else self.strength
                coarsening    = self.bccoarsening    if self.coarsening    == 'auto' else self.coarsening
                interpolation = self.bcinterpolation if self.interpolation == 'auto' else self.interpolation
            else:
                strength      = self.bstrength      if self.strength      == 'auto' else self.strength
                coarsening    = self.bcoarsening    if self.coarsening    == 'auto' else self.coarsening
                interpolation = self.binterpolation if self.interpolation == 'auto' else self.interpolation
        elif iscollection(A):
            strength      = self.cstrength      if self.strength      == 'auto' else self.strength
            coarsening    = self.ccoarsening    if self.coarsening    == 'auto' else self.coarsening
            interpolation = self.cinterpolation if self.interpolation == 'auto' else self.interpolation
        else:
            strength      = self.dstrength      if self.strength      == 'auto' else self.strength
            coarsening    = self.dcoarsening    if self.coarsening    == 'auto' else self.coarsening
            interpolation = self.dinterpolation if self.interpolation == 'auto' else self.interpolation

        S = strength(A)
        cfsplit = coarsening(S)
        
        if isblock(A) and not b=='all':
            # set all variables not in the disired blocks to be 'C'
            b = [b] if np.isscalar(b) else b
            assert all(b_ in range(A.bshape[1]) for b_ in b)
            any_left = False
            blocks = cfsplit._blocks
            for b_ in range(A.bshape[1]):
                if b_ in b and sum(blocks[b_,0][:,0] == 'C') >= self.sbNmin:
                    if sum(blocks[b_,0][:,0] == 'F') > 0:
                        any_left = True
                else:
                    blocks[b_,0] = np.full(blocks[b_,0].shape, fill_value='C')
            cfsplit = kron.block(blocks)
            if not any_left:
                return False
            
        P = interpolation(A,S,cfsplit)    
        
        if isblock(A):
            cfsplit = kron.vstack([(cfsplit._blocks[i,0][:,0]=='C').reshape(-1,1) for i in range(cfsplit.bshape[0])])
            
            if A.iscollection() and hasattr(interpolation,'onlydiag') and interpolation.onlydiag:
                # anderenfalls wächst die collection mit lauter unnötigen Null-matrizen
                assert A.sbshape[0]==A.sbshape[1]
                M,N = A.bshape
                blocks = np.empty((M,N), dtype='object')
                for i in range(M):
                    for j in range(N):
                        blocks[i,j] = P.T._blocks[i,i] @ A._blocks[i,j] @ P._blocks[j,j]
                return {'P':P, 
                        'R':P.T, 
                        'A':kron.block(blocks), 
                        'C':cfsplit}         
                
        return {'P':P, 'C':cfsplit}
            
            
    def _todense(self, A):
        if isblock(A):
            blocks = A._blocks
            for i in range(A.bshape[0]):
                for j in range(A.bshape[1]):
                    blocks[i,j] = self._todense(blocks[i,j])
            A = kron.block(blocks)
        elif iscollection(A):
            A = MatrixCollection([self._todense(Ai) for Ai in A])
        else:
            A = super()._todense(A)
        return A
    
    def _below_Nmin(self, A):
        if iscollection(A):
            for Ai in A:
                if super()._below_Nmin(Ai):
                    return True
        else:
            return super()._below_Nmin(A)


######################################################
# kron matrix
######################################################     

class AMGKronFactory(AMGFactoryInterface):
    Nmin = 50
    
    def options(self, kfactory=AMGFactory(), Nmin=None, todense=None):
        """_summary_

        Parameters
        ----------
        kfactory : _type_, optional
            _description_, by default AMGFactory()
        Nmin : _type_, optional
            _description_, by default None
        todense : _type_, optional
            _description_, by default None
        """
        super().options(Nmin,todense)
        if kfactory is not None:
            self.kfactory = kfactory
            
    
    def _assemble_layer(self, A, k, b='all'):
        assert iskron(A) or iskronsum(A) or iskronblock(A)
        kdim = A.kdim
        
        if isinstance(self.kfactory, AMGFactoryInterface):
            kfactory = kdim * [self.kfactory]
        else:
            assert len(self.kfactory) == kdim
            kfactory = self.kfactory
            
        k = [k] if np.isscalar(k) else k
        assert all(0<=_k<kdim for _k in k)
        
        # build the new matricies for each layer
        data = {'A' : kdim * [None],
                'R' : kdim * [None], 
                'P' : kdim * [None], 
                'C' : kdim * [None]}
        
        any_found = False
        
        for _k in range(kdim):
            A_k = A.getklayer(_k)
            
            data_k = kfactory[_k].assemble_layer(A_k, b=b) if _k in k else False
            
            if data_k:
                # use new coarse grid if existent
                any_found = True
                A_k = data_k['A']
                R_k = data_k['R']
                P_k = data_k['P']
                C_k = data_k['C']
                
            else:
                # else use the identity projections on this layer
                shape = A_k.shape
                R_k = tosparray(scipy.sparse.eye(shape[1]))
                P_k = tosparray(scipy.sparse.eye(shape[0]))
                C_k = np.ones((shape[1],1), dtype=bool)
                
                if isblock(A_k):
                    # block the identity projections if neccessary
                    R_k = kron.block(R_k.tocsr(), sbshape=(A_k.sbshape[0],A_k.sbshape[0]))
                    P_k = kron.block(P_k.tocsr(), sbshape=(A_k.sbshape[1],A_k.sbshape[1]))
                    C_k = kron.block(C_k, sbshape=(A_k.sbshape[1],1))
                    
                    if A_k.iscollection() and hasattr(kfactory[_k],'bcinterpolation') and hasattr(kfactory[_k].bcinterpolation,'onlydiag') and kfactory[_k].bcinterpolation.onlydiag:
                        # anderenfalls wächst die collection mit lauter unnötigen Null-matrizen
                        assert A.sbshape[0]==A.sbshape[1]
                        M,N = A.bshape
                        blocks = np.empty((M,N), dtype='object')
                        for i in range(M):
                            for j in range(N):
                                blocks[i,j] = R_k._blocks[i,i] @ A_k._blocks[i,j] @ P_k._blocks[j,j]
                        A_k = kron.block(blocks)
                    else:
                        A_k = R_k @ A_k @ P_k
                    
                    
                                
            # komische zerlegung bei MatrixCollection raus und einfach append. Für eye bleibt berechnung gleich.
            # bei sum: als collection für jede collecten R@Ai@P und das dann summieren
            # bei only diag A_k neu berechnen über schleife
            
            
            data['A'][_k] = A_k
            data['R'][_k] = R_k
            data['P'][_k] = P_k
            data['C'][_k] = C_k
            
            
        if not any_found:
            return False
        
        # combine the seperate layers to one Kronecker shaped matrix
        if iskronblock(A):
            data['A'] = kron.blockedkron(data['A'],copy=False)
            data['R'] = kron.blockedkron(data['R'],copy=False)
            data['P'] = kron.blockedkron(data['P'],copy=False)
            data['C'] = kron.blockedkron(data['C'],copy=False)
        else:
            if iskron(A):
                data['A'] = kron.kron(data['A'],copy=False)
            elif iskronsum(A):
                data['A'] = sum(kron.kron([data['A'][k][i] for k in range(kdim)] ,copy=False) for i in range(A.N))
            else:
                raise ValueError(f"Unknown type {type(A)}")   
            data['R'] = kron.kron(data['R'],copy=False)
            data['P'] = kron.kron(data['P'],copy=False)
            data['C'] = kron.kron(data['C'],copy=False)
            
        return data 
        
    
    def _todense(self, A):
        if A.shape[1] < self.todense[0]:
            A = A.toarray()
        return A

    
######################################################
# auto assemble
######################################################

def auto_assemble(A, Lmax, factory=AMGFactory(), bmode='sequentially'):
    if isinstance(A, MultiGrid):
        amg = A
    else:
        amg = AlgebraicMultiGrid(A)
    
    if bmode == 'sequentially' and isblock(A):
        N = A.bshape[1]
        while len(amg) < Lmax:
            any_new = False
            for b in range(N):
                data = factory.assemble_layer(amg.A[-1], b=b)
                if data:
                    any_new = True
                    amg.add_layer(data)
                    if len(amg) == Lmax:
                        return amg
            if not any_new:
                return amg
    elif bmode == 'all' or not isblock(A):
        while len(amg) < Lmax:
            data = factory.assemble_layer(amg.A[-1], b='all')
            if not data:
                return amg
            amg.add_layer(data)
    else:
        raise ValueError(f"bmode={bmode} not known")
        
    return amg


def auto_assemble_kron(A, Lmax, factory=AMGKronFactory(), kmode='sequentially', bmode='sequentially'):
    assert iskronrelated(A)
    kdim = A.kdim
    
    if isinstance(bmode, str):
        bmode = kdim * [bmode]
    
    if isinstance(A, MultiGrid):
        amg = A
    else:
        amg = AlgebraicMultiGrid(A)
        
    def assemble_b(amg,k):
        if bmode[k] == 'all' or not isblock(amg.A[-1]):
            data = factory.assemble_layer(amg.A[-1], k=k, b='all')
            if data:
                amg.add_layer(data)
                return True
            return False
        elif bmode[k] == 'sequentially':
            N = amg.A[-1].bshape[1]
            any_new = False
            for b in range(N):
                data = factory.assemble_layer(amg.A[-1], k=k, b=b)
                if data:
                    any_new = True
                    amg.add_layer(data)
                    if len(amg) == Lmax:
                        return True
            if any_new:
                return True
            return False
        else:
            raise ValueError(f"bmode={bmode[k]} not known")
    
    if kmode == 'sequentially':
        while len(amg) < Lmax:
            any_new = False
            for k in range(kdim):
                if assemble_b(amg, k):
                    any_new = True
                    if len(amg) == Lmax:
                        return amg
            if not any_new:
                return amg           

    elif kmode == 'all':
        while len(amg) < Lmax:
            if not assemble_b(amg, range(kdim)):
                return amg
        
    else:
        raise ValueError(f"kmode={kmode} not known")
    
    return amg

# auto assemble: generate based on diag:
# - Assemble projectors for full matrix, but for callculating, use only the diagonals of P.
# - Just at the end, rebuild A with the actual projectors
# -> make use of "sideeffects" but prevent them of fucking up the AMG