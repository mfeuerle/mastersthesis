import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).parent.resolve()) + '/../../source/')

import os.path
            
import numpy as np
from time import time
import matplotlib.pyplot as plt


class CallBack:
    def __init__(self, A, b, x_ref=None, solver_name='solver', one_line=False):
        self.iter = -1
        
        self.A = A
        self.b = b
        self.norm_b = np.linalg.norm(b)
        
        self.relres = []
        self.relerr = []
        
        self.x_ref = x_ref
        if x_ref is not None:
            self.x_ref_norm = np.linalg.norm(x_ref)
            self.res = []
            self.err = []
        
        self.endline = '\r' if one_line else '\n'
        self.solver_name = solver_name

        self.runtime = []
        self.start_time = time()
        
        
    def __call__(self, x):
        self.iter += 1
        Ax = self.A @ x
        
        res = np.linalg.norm(Ax - self.b.reshape(Ax.shape))
        relres = res / self.norm_b
        self.res.append(res)
        self.relres.append(relres)
        
        runtime = time() - self.start_time
        self.runtime.append(runtime)
        
        print(f"\t{self.solver_name}: iteration {self.iter:5d}",end='')
        if self.x_ref is not None:
            err = np.linalg.norm(x - self.x_ref.reshape(x.shape))
            relerr = err / self.x_ref_norm
            self.err.append(err)
            self.relerr.append(relerr)
            print(f": err {err : 5.4e}, res {res : 5.4e}, relerr {relerr : 5.4e}, relres {relres : 5.4e} [{runtime:7.2f}s]",end=self.endline)
        else:
            print(f": res {res : 5.4e}, relres {relres : 5.4e} [{runtime:7.2f}s]",end=self.endline)
        
        
    def print_iter(self, value):
        value_ = getattr(self, value)
        plt.semilogy(value_, label=self.solver_name)
        plt.xlabel('#Iterations')
        plt.ylabel(value)
        
    def print_time(self, value):
        value_ = getattr(self, value)
        plt.semilogy(np.array(self.runtime)/1000, value_, label=self.solver_name)
        plt.xlabel('runtime[s]')
        plt.ylabel(value)
            
            
class CallBackLogging:
    def __init__(self, A, b, x_ref=None):
        self.iter = -1
        
        self.A = A
        self.b = b
        self.norm_b = np.linalg.norm(b)
        
        self.x_ref = x_ref
        if x_ref is None:
            print("iter,res,relres,time")
        else:
            self.x_ref_norm = np.linalg.norm(x_ref)
            print("iter,err,relerr,res,relres,time")

        self.start_time = time()
        
        
    def __call__(self, x):
        self.iter += 1
        Ax = self.A @ x
        
        self.res = np.linalg.norm(Ax - self.b.reshape(Ax.shape))
        self.relres = self.res / self.norm_b
        
        self.runtime = time() - self.start_time
        
        if self.x_ref is None:
            print(f"{self.iter},{self.res:e},{self.relres:e},{self.runtime:e}")
        else:
            self.err = np.linalg.norm(x - self.x_ref.reshape(x.shape))
            self.relerr = self.err / self.x_ref_norm
            print(f"{self.iter},{self.err:e},{self.relerr:e},{self.res:e},{self.relres:e},{self.runtime}")
        
        
        