# Moritz Feuerle, May 2022

r""" 
===============================
:mod:`kron`
===============================


Matrix classes
--------------
.. autosummary::
   :toctree: generated/
   
   kron_matrix
   kronsum_matrix
   kronblock_matrix
   block_matrix
   
   
Construction functions
----------------------
.. autosummary::
   :toctree: generated/
   
   kron
   block
   hstack
   vstack
   blockedkron
   diag
   eye
   zeros
   ones
   
   
Type identification functions
-----------------------------
.. autosummary::
   :toctree: generated/
   
   iskronrelated
   iskronshaped
   iskron
   iskronsum
   isblock
   iskronblock
   
   
      
   
Inheritance diagram
-------------------
   
.. inheritance-diagram:: kron._kron
   :private-bases:
   :parts: 2



.. todo:: 
   type hints for all methods
.. todo::
   switch to @property syntax for all attributes
.. todo::
   restructure with block_matrix, sum_matrix, kron_matrix, which can be used to build combinations like kronsum, blocksum, kronblock, kronblocksum
   
   
.. sectionauthor:: Moritz Feuerle, 2022
"""

from . import utils
utils.patch_scipy_array_priority()

from ._kron import *
from ._construct import *
from ._interfaces import iskronrelated, iskronshaped